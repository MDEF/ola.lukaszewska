#Project your intervention into the future


##Iterations

First step of my project is creating a tangible artefact for my intervention - a starter kit for one of the topic, ie. LIVING LIGHTNING, containing tools and live matter to create a bio-design experiment in a "teacher-less" mode. Secondly, after gathering feedback and implemanting necessary changes, especially into the instruction and syllabus, I want to create a series of workshop that will be directive and guided in their character but will also boost and stimulate thinking about the outcomes by youths (and so they can be guided to come to their own conclusions) working collectively (so a situation change from *one person - one set* to *collectively working mini group - one set*). In the next iterations I want to use the knowledge and experience I gather to omit the direct "kids workshop" step and focus more on developing the kit to make it a tool for helping educators spread the scientific thinking, in a way educating the educators, sharing the tools, giving them the knowledge and implements, which makes the whole undertaking way more powerful and sustainable in a way.


![](assets/iterations-03.jpg)

![](assets/iterations-04.jpg)


##Scientific Thinking Class

Simons Foundation[^7557] gives a definition of scientific thinking: a decision-making process that is driven by curiosity, contingent upon asking questions, and informed by reliable evidence. I imagine that in the future students will attend class called "scientific thinking". Scientific thinking class is a workshop on creating a dialog between fields like natural science, social science, formal science and design using hands-on experience and stretching creative minds. It's a class where not the answers are important but the question, where the process is celebrated.

*The class starts with summing up last week workshops - what did we learn? The projects outcomes are presented. Last week the students were working on a small electronic devices powered by renewable source of energy - moss. Next week the group will work on connecting those devices together in order to make bigger generator. The teacher speaks with students about the process, students are explaining the obstacles they faced. During the workshop hours kids will work with tutors on resolving the issues.*

*It's time to start todays topic: biolight. The lecture starts with short introduction to "microbial home" - a concept that home become more self-sufficient and less wasteful of natural resources. Kids are encourage to brainstorm the lightning concepts in general. The group speaks about the purpose of light in homes, the importance of it. As a contrast, they imagine a life without light. By understanding darkness, they give more meaning to the phenomenon of light. The biology teacher takes over - she introduces briefly the world of bacteria in context of our everyday life to give the basic understanding but quite quickly moves to starting experiments with kids.*

*Kids, divided into groups, sit down by their labs stations. Wet Labs are a necessity in every school, from kindergarten to college. Every group receives a culture of bioluminescence bacteria. They learn for the first time how to cultivate them on a previously prepared agar medium. Every group creates few samples and lays Petrie Dishes in incubators. It takes some time for the bacteria to grow and glow.*

*During next meeting the class will received 3D printed objects. That's the project that older class has been working on - they designed their objects for homes and have them printed in biomaterial. 3D designing is nowadays pretty much like a drawing skill - a common knowledge and widely used technique. The younger pupils will have to implement their bioiluminescence bacteria into the object.*

*The class is finish by talking about what question emerged during the workshops and lectures. Teachers disscuss for a while, give a short description about next meeting that takes place just in the next week and the class goes for a break before their IT workshop begins.*


_________

FabKids (https://fablabbcn.org/fab_kids.html)
Nest Makerspace (https://nestmakerspace.weebly.com/about.html)
GIY Bio Buddies (https://giybiobuddies.weebly.com/)

[^7557]: Simons Fundation. "About", online: https://www.simonsfoundation.org/outreach/science-sandbox/about/ (access: 01.05.2019).
