#Computer aided design
____________
#Software

Some of the software for **2D drawing** together with basic know-how and know-what can be found [here](http://fabacademy.org/2018/docs/FabAcademy-Tutorials/week2_computer_aided_design/2d_tools.html).

Some of the software for **3D modeling** together with basic know-how and know-what can be found [here](http://fabacademy.org/2018/docs/FabAcademy-Tutorials/week2_computer_aided_design/3d_cad_tools.html).



_________________
##123D Make

![](assets/123D01.JPG)
![](assets/123D02.JPG)
![](assets/123D03.JPG)
![](assets/123D04.JPG)
![](assets/123D05.JPG)


_________________
##Grasshopper

Here is one of the project I took part in during parametric workshop. We used Grasshopper's plug in Kangaroo to fid a form by mimicking gravity forces by defining anchor points for the structure. After being glad with the general form, panelised it, then created ribbing, points for connecting parts (panels) with zip locks and at the end number it so we could now which panels go where and rescaled it and projected it into planar that was our available sheets of material to work with, so we could have a ready CNC file. It sounds quite simple and linear, but the whole group together with tutors spend almost full 72 hours with rather quick breaks and litres of coffee, going back and forth few times to actually develop the script and files, living small amount of time to eventually build a structure.

![](assets/parametricfab01.JPG)
![](assets/parametricfab02.JPG)
![](assets/parametricfab03.jpg)

I find Grasshopper to be quite an useful tool - from repeating simple shapes, through data visualisation and form finding to constructions, optimisation and environmental analysis.

__________________________
##Object in 2D

For my 2D illustration thing I choose to run a Grasshopper script, imitating a differential growth (more can be read for example [here](https://www.sciencedirect.com/science/article/abs/pii/0098847289900348)), which is basically a pattern on certain growth. Nervous System also published a pretty nice sum-up of the topic ([go to article](https://n-e-r-v-o-u-s.com/blog/?p=6721)).

<iframe src="https://player.vimeo.com/video/130977932" width="640" height="360" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
<p><a href="https://vimeo.com/130977932">Floraform</a> from <a href="https://vimeo.com/nervoussystem">Nervous System</a> on <a href="https://vimeo.com">Vimeo</a>.</p>

To create the script I followed instruction I once found on Grasshopper forum. I sadly cannot credit the author, for which I am sorry, but here is the shoutout to him/her. The principle is .... . Once the loop is finished, the curves can be fillet and baked into Rhino.

![](assets/2Dgrowth.gif)

Next, I exported the curves in .ai (Adobe Illustrator) and .dxf (AutoCAD) formats, both scalable vectors, making it possible to work further with them in a graphic-oriented software. In Illustrator, I changed the thickness of the curves and added the background. At the end, I exported the file as a JPG picture.

![](assets/2Dgrowth1.JPG)

![](assets/2Dgrowth3.jpg)

I hope to use those curves in one of the next assignments, either in a laser or maybe a casting and molding week.  

**DOWNLOAD: [HERE](https://gitlab.com/ale_lukaszewska/docs/tree/master/docs/FabAcademy/week02)**

_______________
##Object in 3D

At first, I thought about creating my object in, as usually, Rhino with the side support from Grasshopper. But that is a good occasion to actually test different software which I am normally not used to.

For this assignment, I got a support from [Patrick Birke](https://www.instagram.com/autozeichner_design/), transportation design student. The most common software for 3D modelling in car design industry is Alias, followed by Maya, VRED and according to some ranking lists, also 3DMax and Blender.


**ALIAS**

First, I tried Alias - mostly because it has a Dynamo, sort of "Grasshopper for Alias", parametric tool. For those who know Grasshopper, it might be way easier, as the workflow and logic are more or less the same. There are some "dictionaries", showing similar commands in both Grasshopper and Dynamo, like [this](https://parametricmonkey.files.wordpress.com/2016/01/dynamo-for-grasshopper-users.pdf) one. It takes a while to get used to new interface and moving around the program, but once that part is done, I can start creating a script.

![](assets/alias01.PNG)

What I find really useful, especially for a rookie user, is a reference library, where examples, explanations and even scripts can be found, desciribing every component.

![](assets/alias02.PNG)

![](assets/alias03.PNG)

At first, I did a really "basic" thing that I learned during one of my first Grasshopper classes. It's a pattern created by an attractor point - the radius of circles placed in a grid is changing depending on the distance from given point (or curve, but that changes the script just slightly). Secondly, I extruded the circles in Z direction depending on the size of the circle.

![](assets/alias04.PNG)

![](assets/alias05.PNG)

![](assets/ALIAS23.PNG)

I eventually moved to something containing more operations. I watched [this](https://www.youtube.com/watch?v=_na68eG_jj4) webinar and used some steps from [A. Samosonowicz's](https://www.youtube.com/channel/UC468uWfsaD8lEcQVlHU7qHA) videos.

![](assets/ALIAS21.PNG)

At the end, I didn't complete the geometry, as I had some troubles with Dynamo itself - the script that was working basically wasn't showing the geometries (and it wasn't any of the obvious reasons, like the preview that I accidently turned off; restarting neither Alias, Dynamo nor the whole system didn't help).

**DOWNOLAD DYNAMO AND ALIAS FILESE [HERE](https://gitlab.com/ale_lukaszewska/docs/tree/master/docs/FabAcademy/week02)**


**MAYA**

Next software I tried was Maya. I didn't spent much of a time on it and followed some short instructions, but generally speaking it seems like quite a tool especially for more "imaginary", freestyle modeling. Maya can be use for animation, modeling, simulation, and rendering.

![](assets/maya22.PNG)
![](assets/maya23.PNG)

Maya works on polygons, NURBS, and subdivision surfaces - with different ways of modeling. Polygons let model a surface by building up and reshaping a number of simple surface facets. NURBS let create smooth, curving surfaces with high-level control. Subdivision surfaces let edit surfaces at a high level with minimum overhead data, while still letting you work with subsections of the surface as if they were made from polygons.

![](assets/MAYA31.PNG)
![](assets/MAYA32.PNG)
![](assets/MAYA33.PNG)

Eventually, I gave Maya a spin with quite "free-style" play - I started from a sphere, edited vertices, faces and edges by rotating, scaling and moving them.

![](assets/maya25.PNG)
![](assets/maya26.PNG)

All in all Maya seems like a software I would like to explore more. For part building I will stick to either Rhino or Solidworks, but for formfinding, animation and even some graphic elemets Maya is fun to play around - at least as far as my current needs go.


**DOWNOALD MAYA FILE [HERE](https://gitlab.com/ale_lukaszewska/docs/tree/master/docs/FabAcademy/week02)**
