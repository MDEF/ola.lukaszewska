#**WEEK 10. An Introduction to Futures**
*by Elisabet Roselló Román*


____
##The impossibilities

Doesn't an impossible exist? In mathematics it does. The angle trisection problem describes the (im)possibility of dividing the angle into three parts using only two tools - an unmarked ruler and a compass. Another example is the quadrature of the circle - the problem of constructing a square whose field equals the field of a given circle using only a compass and a ruler without a scale - another impossibility. The art of mathematics also describes impossible random events, i.e. events that have exactly zero chance of occurrence - for example, pulling a red ball out of the bag in which there are only black balls or drawing number 7 on the dice numbered 1 to 6.

In the field of physics, the statement that something is impossible is a risky statement. Generally speaking, physicists assume that there is no certainty that we will know with complete accuracy about the pure nature of things or that we will know all the secrets of the Universe. They are aware of their "unknowing". Physics advocates a gradual approach to explaining and perceiving boundaries as barriers that aren't to stop but to break. Therefore, impossibilities in physics are sometimes divided into three categories, and none of them presupposes the absolute absence of a given solution. For example, type I are solutions that aren't achievable today, but are not in contravention of the known laws of physics - for example invisibility, psychokinence or teleportation. Type III are solutions that are contrary to the known (and that's were the emphasis goes - to the known today; achiving those designs would require rewriting the whole physic as we know it) laws of physics, but not completely excluded - surprisingly few concepts fall into this category (for example, perpetuum mobile).

Michio Kaku, a physicist, in his book "Physics of the impossible" points out that the study of impossible things can change the course of history. He cites an anecdote about an atomic bomb: in the 1930s (including Albert Einstein) it was a common believe to consider as impossible to construct an atomic bomb. After a series of events that include Leo Szilard reading "World set free" by H.G. Wells, led to the research in strengthening of the chain reaction between individual atoms. In the end, in 1933 a series of experiments was carried out. This influenced the launch of the Manhattan Project and, as a result, the construction of the atomic bomb. The study of impossible things opens up new perspectives - pushes the limits of physics.


____
##The paleo future

Physicists investigate the impossibility of the future, but also try to understand the past that goes back to the Big Bang. On the linear concept of time they are busy with both events on the left (past) and on the right (future) side - both directions are known and unknown at the same time (in the meaning of informations and understanding). Today we know what we know. Our tools in predicting trends of the future or the possibility of certain phenomena are becoming more and more advanced. It is always interesting for me to read such predictions from a dozen or so years ago, when physics, technology, information availability and - I will risk stating that - even consciousness  weren't on the same level as today.

*TVs that hang on walls? Automatic lights? Food cooked in seconds? American power companies sure had the future figured out! Except for one little thing... we're still waiting on those driverless cars. ELECTRICITY MAY BE THE DRIVER. One day your car may speed along an electric super-highway, its speed and steering automatically controlled by electronic devices embedded in the road. Highways will be made safe -- by electricity! No traffic jam.. no collisions... no driver fatigue.* (http://paleofuture.com/blog/2010/12/9/driverless-car-of-the-future-1957.html)[Paleo Future]


![](assets/car.jpg)


*In his 1986 book July 20, 2019: Life in the 21st Century author Arthur C. Clarke discusses what sexual relations will be like in the year 2019. He envisions a world in which people "boldly state their desires, no matter how bizarre or specific."* [http://paleofuture.com/blog/2009/6/28/sex-in-the-year-2019-1986.html](Paleo Future)


![](assets/sex.jpg)


Today, by looking at these visions, we can identify some germs of some projects from today. From back then we can already see dialogues on drones, trips to space, wireless telephones and many more - it's visions of technology, facilitating processes, simplifying everyday activities, and expanding borders.
Another part of the stories from the 30s, 50s, 60s ... are very colorful visions of earning without work, six-year drivers and tennis racquets that hit the ball themselves. In these years, a popular exercise at schools was to write a letter to "people from the future". In addition to writing about free sweets, there were many references to wars, air pollution, and stable economy. Such letters were full of hope for a better tomorrow - they were a reflection of those days. They were a mirror to their times. Examples: [https://paleofuture.gizmodo.com/18-bizarre-letters-to-the-future-that-only-70s-kids-wil-1451498047](here) and [http://paleofuture.com/blog/2009/4/8/stupid-kids-imagine-the-year-2075-1975.html](here).


___
##Futures

The wishes of a given epoch, the hopes for the better, and the fears of worse are reflected in literature, film, and art. There is a whole mass of more or less accurate descriptions of the futures, utopias and dystopias (the list will be included at the end of the reflection as "references"). But whether in literature or film, as in physics, it is about the accuracy of the theorem? Or rather about letting the imagination fly, question what is given and recognising the agents of changes?

What is then the role of future impossibilities and future scenarios in design? How do we speak about the futures? Future studies are not about predictions - those studies don't assume and don't base the theories on those - there is no particular, one and only future. The future is unique. Therefore it's somewhat better to speak about forecasting than predicting.

Future scenarios are the stories about future that describe how a future occurrence comes to be and what could be the effects. It's a design tool. There are some ground rules for creating those, as listed:

- Scenario has to be consistent - all the parts put in the story must reinforce each other, they must connect with each other. Factors must be linked.
- It has to be coherent - again, items must match.
- It cannot be "plausible" - if as an answer to scenario one hears "yeas, that is actually quite possible!", it's probably not a really good scenario because it sticks to much to the present and to what is known nowadays.
- It has to question the status quo - questioning things that are taken for granted nowadays, not assuming things are meant to be in particular way, asking the "what ifs": what if this change, collapse, stops, won't work.

Every time by introducing new factor to the scenario the following things should be checked:

- what do we know about current change and the present situation?
- what do we not know about them?
- therefore, what do we need to learn about them?


After all, we don't write the scenarios to "get it right". It's not a construct of predicting the right future, telling how it's really going to be. The aim of speaking about (and designing for) futures is to let us understand the consequences of some particular choices that may be done and therefore noticing, spotting and describing linked actors of changes - seeing the bigger picture.

The least likely future is that in which nothing has changed.
