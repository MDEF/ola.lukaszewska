#Design Dialogues II

![](assets/biotitle.gif)


__________________________
##Presentation


There are issues from our times, in a global scale, like environmental changes, pollutions, the problem of sustainability and way more, that needs to be address urgently. They are not weak signals of future changes, those are huge problems of today. I believe that by empowering future generations with biology literacy we can work against those (and other) changes by giving todays children a tool of critical thinking, by "preparing" generation of the children that not only understand the mind shift, but they have their mind shifted from what we know now, understand the importance and role of nature in a way that we are nowadays starting to realize, that they understand the damages we are doing or have done and can act against it or ideally, in next iterations, not do them at all and think about consequences at the first place. And so my intervention is aiming at the educational gap that is happening nowadays by designing an accessible kit for the experience with bio/design, not for the sake of science or biology learning, but to show that biology and design combined can create some thinking changes and can be used as a future solutions for at least some of the words struggles.

![](assets/boxpreview.jpg)

To help me speak about the project and communicate easier, to support the narratives and visualise the outcomes for the Design Dialogues session I prepared a mock-up of the kit. Technically, it was a box with possible outlook, but as an speculative artefact, it was a starter pack for a bio/design experience - transferring the thinking process in (soon-to-be) objectified physical experience. The box was placed along with Arduino kit and Micro Bit set, as I want to use similar analogy as it is used in those two starters - the design of the box (and by that I mean the full package, that is what tools are inside, what is the project about, the handbook, guidelines, a word to "supervisor", learning outcomes, syllabus etc) is just a first iteration of the project for me as a designer and also a first step to start with biodesign for a person using it. Following the handbook given in the set allows the user to create a project, but the components, once someone feel confident enough, can be use into further biohacking experiments. Ideally, the next step would be to create a series of those tools (boxes), tackling different areas of biodesign, different issues, having different learning outcomes. Subsequently, creating a platform for learning is a goal, as it is done in both Arduino and Micro Bit cases - because smartly done platform can gather an impactful, insightful community around. Eventually, where I see this project, the whole process should focus on helping educational community, similarly to what happened with FabKids - educating the educators, sharing the tools, giving them the knowledge and implements, which makes the whole undertaking way more powerful and sustainable in a way.

![](assets/iterations-03.jpg)

![](assets/iterations-04.jpg)

As far as the children age goes, I still have to dive into the development, needs and possibilities adequate to age ranges, but as a first sketch inbetween the lines, I see the first set to be for more teenage target group and with every next iteration, going younger and younger and ultimately, in the future, introducing biodesign as early as possible, which hopefully will create the mindest shift.  

While writing this post I started to think about other models of biodesign presentations. Workshops, organized outside of school time, with the person conducting, supervising the process and results, who can summarize the activities, are a certainty. They are also a kind of bridge between the launch of the box and the planned future education of teachers. But in the meanwhile, what came to my mind is the idea of ""​​POP UP BIODESIGN LAB", which somehow took up a visit to Bicyclot as part of the Make Here project together with the images that stuck in my head after my studies in Poznan - mobile coffee trolleys. I think that this is a concept that I will consider in the near future of this project. Of course, not as a substitute, but as an additional form of revising the topic. Each of these models, whether it's a workshop, a trolley or a box, is governed by slightly different laws and requires a unlike approach to the subject, but the main, fundamental principle remains unchanged - it is about presenting the biodesign in children's culture.

________________

![](assets/A3_POSTERS-01.jpg)

**DRIVERS OF CHANGE: FUTURE OF LEARNING**
I haven't yet fully described the weak signals that will be meaningful for my area of intervention, but upon my research I came across the report by KnowledgeWorks, [Thinking About the Future of Learning](https://knowledgeworks.org/resources/forecast-5/) which I used for my presentation as it shows the main areas of weak signals in education and learning. It's a start point. More can be read under the link above. KnowledgeWorks in their report described also a list of provocations for the future learning, that is future possibilities that could help support the healthy development of children, enabling effective lifelong learning and contribution to community vitality with the main provocation themes stating possible features of the future teaching and learning outcomes, along with *amplified voice and impact*, reconfiguring engagement and outcome frameworks to bolster individual capacity and to increase community impact or *signature learning ecosystem*, situating learning in place in ways that integrate technology, culture and learner and community identity to enhance and extend opportunities for learning.

_____________

![](assets/A3_POSTERS-02.jpg)

**STATE OF THE ART: BIOLOGY BY DESIGN**
As one of the suggested exercises, I did a list of state of the art, a compilation of recent developments, projects and undertakings around the field of my interest area, writing down all the components that come into those works. The goal is to create a list of elements that are used in those projects and by taking the ones that are valuable, can contribute to my project, can be parts of my kit and with those create a syllabus and therefore, the bio/design kit, as shown below - a diagram sketched by Jonathan Minchin, explaining how this exercise is supposed to work.

![](assets/THESTATEOFART-02.jpg)

For the next step the full list should be divided into clusters, creating the categories of components, putting tools, materials, learning outcomes, motivations etc. in separated boxes rather than in one column.
____________

![](assets/A3_POSTERS-03.jpg)
**ONE OF THE BASIC WHYS**


____________
##Video

We were asked, as one of the requirements, to present the video along with the exhibition. I wasn't really sure how to show my process, so I prepared a looped animated presentation of my resources and inspirations, together with keywords and bulletpoints that are a guidance for me. Seeing other colleagues' videos and how they approached the topic, gave me different understanding of that part of the presentation. It's a way of story telling but also an exercise that can come in handy, as it's yet one more way of narrating the project. So my first attempt is a *input video*, combining some of the things I've been reading up, looking into and asking myself.

_____________
![](assets/inputvideo.gif)
________________


I will post soon here the video that hasn't been shown during presentation, but is a sort of *output video*, showing the direction of my project, its description and the goals.



______________
##Feedback

**THE CHALLENGES OF NARRATIVES**
The biggest challenge for me and also the most recurrent point of feedbacks was about narration of the project. The way you show it may affects what kind of feedback you will get, of course what will be understood by the other person. The narrative of the presentation and project shows what core values are being shown. And the meetings like that one - having dialogues instead of "classical" presentation - allow to test different approaches to narratives. It's always good to think about the description of the project like it needs to be said to someone who sees it for the first time, having all the goals and learning outcomes stated really clearly.

To create narratives for delivery at the end of the course it's necessary to know all the components that describe the scenario - the whole "project", meaning in this case a physical artefact, the kit doesn't have to be fully develop but the scenario, the intervention point and goals have to be out clearly, defined.


**STRUCTURIZING WORK**
For the next trimester it will be really important to structure the work and that means creating the plan. The trimester will held 6 weeks and it's important to fit the project into those time constrains. The possible plan is to develop a functioning prototype focusing maybe on one part, one component and then present the vision of possible outcomes and future implications, showing how the project may evolve, what are the next possible steps, what impact do I expect it to have.

Another topic in organising the work is how to structure the biology content, which in case of this proposition is crucial part. I have to extract the possibilities and at some point really narrow them down in order to be able to choose one or two focus point for my first iteration of the kit. To do all that, I may need to reduce my research to kingdoms of bio-organisms, create material and tool sheets - to work on cross-sections of those lists, defying what's essential, what may be needed and at the end, designing the experiments together with learning outcomes.


**DEFINYING THE AGE**
Age-wise toys can stimulate the brain of a toddler, pre-schooler, teenager. Different ages comes with different understanding of abstracts, variety of developed motor skills or the senses and therefore different needs. Every age group has its own objective, especially when it comes to learning and exploring the world. Therefore, defying the age of targeted audience is crucial in order to maximize empowerment coming out of the design of the kit.


**COMMUNICATING SCIENCE**
How someone as a parent comes across the kit? What's a message and how to make it clear? That's an important question to be considered. If the teacher is present and is trained, it's easier to make sure that the learning outcomes are properly understood. But in case of such a "use at home" box, I have to put a lot of effort in making sure that outcome message is universal in a sense that it can be properly understood by any possible parent who can help then guide the children (or if the children is old enough, that it's understood by the given). So at the end the question is: *how to communicate science?* - for both parents (or any other supervising person) and children - to be put clearly, understandably, to have clear instruction, not to be discouraging but rather playful and engaging already at the very start. Because from how I communicate it comes the fundamental thing - what does the children get out of this box. If that part fails, I will create just another set with things inside - maybe fun to play with, but not valuable. Liz Corbin, from whom I got this part of the feedback, used this on-point scale that I allowed myself to redraw below:

![](assets/letsmake.png)

Properly communicated "whys" aka learning outcomes and the importance of bio/design are giving the third, fundamental, dimension to the scale, where the optimal point is between a kit that allows you to make things, a kit that teaches about science and with new dimension, the firm, solid but transparent why.


**THE UNIQUE VALULES**
It could be useful to make an exercise and check out other kits on the market, similar or analogous, taking a look at what does and what doesn't work in them and later, having that list in mind, thinking about the unique value of my future set - what is that one thing that will make it pop out among others, that will make it easy to be chosen - what is the unique value of it, what makes it different from what is "already there"? It's not only crucial to know that, but even more necessary is how to communicate it and how to create the narration that will underline that value - I may know what it is, but I have to find a way to clearly show it.


__________________
##6 week plan

6 week plan of the intervention is a schedule suitable for time constrains we have considering this master course. In 6 weeks we have to present our area of intervention, our project and possible future developments and effects, as far as I understood. I don't plan to close the topic with the submission of the thesis, but rather continue it, which I have to say is actually a quite satisfying feeling, knowing what is my main plan, my interest line for those next 5 years we were asked about during first week at the bootcamp, where I had completly no idea. Coming back from the offtopic, here is a first draft of the plan.


<iframe width="640" height="360" src="https://miro.com/app/embed/o9J_kxtCLM8=/?" frameborder="0" scrolling="no" allowfullscreen></iframe>


I know the timetable will change as I go, it will change once faced with classes schedules and how they will contribute and reflect in my own work, but it is a base to have a start point, adjustable with time.

[Here](https://ibb.co/2yMXzxC)] is the mind-map I draw trying to find a process and components of what do I need to do and therefore what elements need to be scheduled.

<a href="https://ibb.co/2yMXzxC"><img src="https://i.ibb.co/g3Ws0Nf/mindmap-01.jpg" alt="mindmap-01" border="0"></a>

[Here](https://ibb.co/wgCKTLG) is a mind-map of disbanding my area of interests / my inital question and areas of intervention answering to some of the weak signals.

<a href="https://ibb.co/wgCKTLG"><img src="https://i.ibb.co/7CVGHzM/mindmap-02.jpg" alt="mindmap-02" border="0"></a>

____________
##Intervention

My intervention orbits around topics such as **SCHOOLING SYSTEM, SCHOOLING METHOD, BIODESIGN, BIOLOGY BY DESIGN, (BEYOND) BIOMIMICRY, CLOSING THE EDUCATIONAL GAP, CHILDREN'S CULTURE** and some more that are presented in the diagram below (click to see the full image). Meandering through last term, together with FabAcademy, was somehow a tricky thing, not to loose the initial idea, but also obviously not to stuck to it too much and let the project develop and adapt, especially during classes like Atlas of Weak Signals. It helps incredibly to create the framework of the project and define the areas of intervention way better.

To see my intervention elements, click below.


<iframe width="640" height="360" src="https://miro.com/app/embed/o9J_kxyg4vM=/?" frameborder="0" scrolling="no" allowfullscreen></iframe>


____________
_____________

Icon list:

- Science by Mahesh Keshvala from the Noun Project
- Craft by priyanka from the Noun Project
- problem by win cahyono from the Noun Project
- Tool by vectlab from the Noun Project
- boy by Mat fine from the Noun Project
- girl by Nook Fulloption from the Noun Project
- Teacher by Lola Van Trinh from the Noun Project
- Arrow by David Alexander Slaager from the Noun Project
