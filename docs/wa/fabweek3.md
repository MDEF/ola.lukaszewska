#Computer controlled cutting

____________
##Laser cutting

Laser cutting is a technology that uses a laser to cut materials. Laser cutting works by directing the output of a high-power laser most commonly through optics. The laser optics and CNC (computer numerical control) are used to direct the material or the laser beam generated.  [(*source*)](https://en.wikipedia.org/wiki/Laser_cutting)


In our and most of the workshops a "waste" box can be found - it can come in handy especially if you need to run few trial cuts or engravings first or if the piece you need can fit into the sheet that for someone else was already a scrap. It can save a lot of trips to material supply stores.

![](assets/photolaser.jpg)

![](assets/photolaser1.jpg)


It's quite convenient to have some most popular material samples near the laser, as it can spare a lot of time trying to figure out the proper speed and power. First trials on a material should be done anyway, but they usually oscillated around given parameters, so it's way easier to try the perfect balance between frequency, power and speed.

![](assets/photolaser2.jpg)

_____________________________________________________

One of the interesting outcomes from cutting is coming from a flat sheet of material into 3D shape. It can be achieved by designing an intersecting pieces, of course,  but another way to do that is through cuts on the surface. I ran few tests presented below. It's could be a direction for making a flat surface little bit more haptic, for instance, as with the buttons or more flexible or bendable.

![](assets/laser01.gif)

![](assets/laser02.gif)

![](assets/laser03.gif)


_____________________________________________________

##Press-fit kit

**Parametric set up**

![](assets/puzzle01.JPG)

First, I prepared the parametric algorithm in Grasshopper to be able to control the shape. The script is rather "step-by-step" kind of thinking rather than a really tricky sneaky smart solution, but it does serve its purpose.

![](assets/laserpuzzlescript.png)

The parameters that can be adjusted are the thickness of material and therefore the size of intersecting parts (marked as *X*) and the overall size (hight and width - the shape is based on a square) (marked as *A*).

![](assets/laserpuzzlescript2.PNG)

I eventually moved to more manual part, where I baked the curves (on the screenshot below in green), gave a filet on the outside of intersecting elements, so while playing with the pieces the material with sharp corners won't break down (marker in red). I aligned the shapes and placed them on a plane of a size of my material, to make sure it all fits. With such a prepared file, saved in Rhino 5 (.3dm), I moved along to lasercutting ("printing", in the terms of software).

![](assets/laserpuzzlescript3.PNG)


I used following settings on the Trotec Speedy 100R laser:

**POWER: 70  |  SPEED: 1  |  FREQUENCY: 1000 Hz**

The material I was using was a sheet of 3mm plywood.

![](assets/laserpuzzle.jpeg)

The only thing to remember to make those settings work good is to have a calibrated table of the laser, so the focus of light is proper, clean lens and flat layed material (higly recomended using a masking tape on the side to keep it flat, otherwise there might be some places without the "through-cutout").


![](assets/laserpuzzle2.jpeg)

![](assets/laserpuzzle3.jpeg)

Once the set was cut, I could assemble some puzzles.

__________________
**Files**

DOWNLOAD **PRESS KIT** FILES [HERE](https://gitlab.com/ale_lukaszewska/docs/tree/master/docs/FabAcademy/week03/PRESS%20KIT)

DOWNLOAD **TEST LASER CUT** FILES [HERE](https://gitlab.com/ale_lukaszewska/docs/tree/master/docs/FabAcademy/week03/LASER%20TEST)



______________
##Plotter

Plotter is a machine that prints (or cuts) vector graphics continuous point-to-point lines directly from vector graphics files or commands. The blade (knife) moves in X axis while the rollers move the sheet of material in Y axis. It can cut vinyl, paper, cardstock, heat transfers and even some fabrics.


![](assets/plottersticker.JPG)

I started the learning process from being instructed by a colleague and cutting out one circle. Once I was (barerly) sure I can learn on my own and try one of my graphics, I prepared curves to be cut (note: remember to join the curves that can be joined), loaded material and started cutting, trying different sizes of the graphic, learning from mistakes and finally screen printing (more in the next chapter).


![](assets/ploter01.jpg)


![](assets/cutting.gif)


![](assets/ploter004.jpg)


![](assets/ploter005.jpg)


![](assets/ploter006.jpg)


_____
##Screen printing with vinyl stencil

For screen printing, I measured the size of available silk screen, made a frame around my design in order to have a negative stencil and once it was cut, I placed it on the screen, applied paint and printed on both textile and paper.


![](assets/ploter003.jpg)


![](assets/ploter002.jpg)


![](assets/ploter001.jpg)



_________________
**Effects**


![](assets/ploter02.jpg)


![](assets/ploter03.jpg)


_____________
**Files**

**DOWNLOAD FILES [HERE](https://gitlab.com/ale_lukaszewska/docs/tree/master/docs/FabAcademy/week03)**

I attached both .3dm file and .dxf because normally .dxf is universal and should work everywhere, but on the big plotter at our FabLab I couldn't open the file and printed directly from Rhino instead.



________________
**Mistakes**

![](assets/mistakes.jpg)

Along the way of this rather brief experience I was able to make couples of mistakes. Here is a short list of them:

- Files: some of the first graphics I prepared haven't been read by the plotter software - I've tried different extensions, like .svg, .ai, .dxf or .dwg. At the end .dwg was the most suitable one. The thing I forgot about was to set up the right color of lines - that matter is quite similar to laser cutter, where different preferences (if it should be either cutting or just pressing in order to be able to fold etc.) can be assigned to different colors of the curves.

- If the rollers in the plotter are not locked, the paper will start to rotate and therefore the cutting or folding won't be done perfectly.

- While cutting relatively complicated and detailed cuttings, it's good to use the adhesive mat while cutting (default mat included in the plotter kit) or use transfer foil to be able to first apply the whole sheet and then take off the unnecessary part.

- Once again the rule "measure twice cut once" applied - in this case it was not cutting deep enough and letting the knife slide two times over the same curve. The plotter is quite a precise machine (in a sense) but not necessarily twice on the same material - it misplaced the cuts (the material has been moved).

- Screen printing with the vinyl instead of blocking emulsion doesn't make the stencil impermeable to the ink so the printouts are not super precise. The vinyl doesn't stay glued to the screen, but that might be as well the matter of material or screen and then also the paint I used. It would require more tests and tries but anyway it's quite a playful and fast way of making stencils and printing things.
