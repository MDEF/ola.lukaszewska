#3D printing

The assignment: prepare and print something. 3D printing is a process of creating physical 3D objects by joining or solidifing material, controlled by computer. There are different technologies for doing so, most of which are covered for example in this video:

<iframe width="703" height="480" src="https://www.youtube.com/embed/zIP-284Ond0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

___________________
##Files

I prepared the model for the purpouses of Mterial Design Classs - I wanted to have a form to press the material inside creating a thin "block" with a structure on the surface which I created by reworking Grasshopper script for weaving from "AAD Algorithms-Aided Design" book by Artur Tedeschi, later adding walls and thickness in Rhino, ending with a **.3dm file**. Software of the printers, like Cura, reads a files with **.stl** extensions (stereolithography), so for that I needed to export the file and save with this given format. Cura reworks the model and at the end translate it to **GCODE**, a code readable by a printer.

![](assets/3dprocess.jpg)

![](assets/rhinoscreen.JPG)

__________________
##Printing settings

Before printing starts, we have some settings to choose from. They all affect the quality of the print, time of the print etc. Some of the settings that need to be thought of:

- type of material (PLA) - affects for example the temperature needed to melt the material before extrusion;
- nozzle size (0.4mm) - that's the size of the extruder, so also a thickness of one layer;
- height of one layer (0.15mm) - the lower the number, the slower the printing will go
- infill percentage (10%) - depends on the structure, in this case a thick infill wasn't necessary;
- generating support (no) - in this structure not necessary, but if we have elements that are wider than the base (so basically go like "v") or are floating, it's better to have the support, some sort of columns;
- building plate adhesion (yes) - an adhesive surface for the first layer

After "preparing" the file with settings we can see estimated time of printing (3h 13min), travels, shell or infill. If there are some errors or troublesome parts, that's the moment to look for them. Next screenshots presents different settings and how they affect the model and printing time.

![](assets/screensnap01.JPG)

![](assets/screensnap02.JPG)

![](assets/screensnap03.JPG)

![](assets/screensnap04.JPG)

![](assets/screensnap05.JPG)

![](assets/screensnap06.JPG)

![](assets/screensnap07.JPG)

_______________
##Model

The printed model presented (with leftover dirt after Material Design workshop). This kind of smooth soft and dome shapes will always have those layers of print seenable. I didn't mind if for the purpouse of this material exercise, but normally there are few ways to smoothen the surface, i.e. using a spray primer and then wet sandpaper.

![](assets/DSC02691.JPG)

![](assets/DSC02697.JPG)

_______________
##3D scanning

To make a scan, I used the software available on one of FabLab's computer, [Skanect](https://skanect.occipital.com/features/). After launching the program and making sure a camera (Kinect) is connected, all I had to to is choose what I want to scan (a person, an environment, an object), sit on a rotating chair and nicely asked someone to keep turning me around - it's way easier than walking around with Kinect, it's more stable and at the end, everything moves on the same level (unlike the hand that would carry the Kinect, although if you have an electric longboard, a drone, a electric train toy - it all can be improvised ;).

![](assets/skanex.JPG)

![](assets/skanex2.JPG)

![](assets/skanex3.JPG)

![](assets/skanex4.JPG)

At the end, my head had a hole on the top, which I repaired later in Rhino. It also didn't catch all of the details of the face, like nose. In order to solve that, I would probably have to have cleaner scenery and better lightning, to create more contrast diversion in a object like face.

**DOWNLOAD SCANNED HEAD [HERE](https://gitlab.com/ale_lukaszewska/docs/tree/master/docs/FabAcademy/week05/SCANNING)**

__________________________________
##**UDAPTE**

I made a new printout to (relatively) "fully" show the additive possibilities of 3D printing, as the previous one could be made as well by milling machines. The model is pretty simple: a field of columns lighten in the middle, based on a Grasshopper's Lunchbox hexagon grid. It's a potential figure to create future tests on the soft structures, like with springs inside, made in one print with one material - a topic that could go in the direction of shoe soles design, perhaps.

![](assets/3Dstructureprint.jpg)

![](assets/3Dstructureprint2.jpg)



Here are the settings I used for a REPRAP printer:


![](assets/3Dstructuresettings.PNG)

![](assets/3Dstructuresettings2.PNG)

Printing process:

![](assets/reprap1.jpeg)

![](assets/reprap3.jpeg)

Results:

![](assets/DSC04118.JPG)

![](assets/DSC04120.JPG)

The results can differ depending on the printer that is used (a problem that occurred to me during molding and casting week). In the case of REPRAP printer the bed needed to be adjusted by hand, so the first layer can stick properly. It took few tries, but at the end it was working well.

**DOWNLOAD THE STRUCTURE [HERE](https://gitlab.com/ale_lukaszewska/docs/tree/master/docs/FabAcademy/week05/3D%20PRINT)**
