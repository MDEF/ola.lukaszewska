#**WEEK 09. Engaging Narratives**
*by Heather Corcoran & Bjarke Calvin*


____
##Short guide

![](assets/narratives01.jpg)

![](assets/narratives02.jpg)

![](assets/narratives03.jpg)

![](assets/narratives04.jpg)

![](assets/narratives05.jpg)

![](assets/narratives06.jpg)


____
##Introduction

In week 9, we looked at different ways of conducting narration, that is, the way in which we can show and explain. This week is for me, above all, a huge amount of examples of storytelling in general and story telling on Kickstarter, a crowdfunding portal. At the end of the post I will attach a link to playlist with examples of good stories, campaign videos and talks on storytelling.

The above graphics are keywords or key phrases that summarize the tips for creating the narrative. The narrative and tools we use depend on the medium on which it will be published - for example, if we want to tell the story on Instagram, we use very visual tools - photos, graphics. The description, although important, draws attention secondly. Kickstarter has very specific rules for content. Story telling on Facebook could be done by many different ways - for instance only by text or just by short catchy video.

The Kickstarter campaign is nothing else than storytelling. Permanent elements of the campaigns there are:
 
- a video that is in itself a story - works as an invitation to history, to the project
- post, that consists of both a written and graphic part - photos that should be catchy but clear and description that cannot be too generalized and cannot promised things that cannot be delivered
- reward for supporting the project, that could be, depending on the project, the project itself, especially if it has a physical form, acknowledgment or honourable mention, a souvenir object, so an item, but not directly the project or even an experience, like invitation for an exhibition or meeting with artist
- update of the project, which is basically a story line of the process and by sharing it we can give the opportunity to look behind the curtain; this part can also create a very big engagement.

Talks and the presentation about how Kickstarter make me reflect on why people would like to give money in order to support an idea. During the class discussion, we talked about the fact that people can do it for many reasons, of course, but this is mainly because they believe that the idea should be out there in the world, alive. Additionally, there is a need to help in human nature, i.e. people like to be asked for help. This in turn makes them feel involved. They feel like they may be a part of something bigger and greater. Contributing to the campaign gives a feeling of being - even if partly - responsible for the future existence of that idea. On the other hand, some supporters and backers do that as they simply desire the given project.

___
##Duckling

*We evolve by sharing insights. Once it was by word of mouth. Then it became print media and then social media. Now, once more, it’s time for a new kind of media. A media that unites us, instead of dividing us. A media that makes us see the bigger picture, instead of the selfie. A media that makes us evolve. We call this insight media.* That's a description of Duckling, application done by Bjarke Calvin. Duckling, as written, is a tool for creating insightful content. It's a playful tool for creating stories that can provide "insider's" information on any given topic.

[http://duckling.me/interest/emergentfutures](Here) are the insights MDEF class created during Duckling workshop hour.


____
##Cosmetic Biolab

For the final workshop assignment I had to go back to my own area of interest and create a story based on that. Bellow are the stories I prepared. Notes on needed improvements, mistakes and further plans will be written after both presentations.

At first, I encouraged the viewer to take a look at the short book.

<div data-configid="29920998/66315784" style="width:525px; height:372px;" class="issuuembed"></div>
<script type="text/javascript" src="//e.issuu.com/embed.js" async="true"></script>

After that, one could take a look at three samples that were made (using speculative prototyping techniques from the previous week). Objects themselves weren't able to tell a story, although I think they whispered gently about it - the disc actually smelled with lavender, and the "lipstick" changed the color of the lips (and fingers).

<div data-configid="29920998/66316666" style="width:525px; height:330px;" class="issuuembed"></div>
<script type="text/javascript" src="//e.issuu.com/embed.js" async="true"></script>



____
## How we blush

It was only during the presentation that I realised that I had gone away from the subject of a story about designing a laboratory as a space in a house and rather instead dealt with the subject of beauty, naturalness and cosmetics. The book therefore can't tell stories about the world in 2050 - the world is just a background. Giving a narrower context may be more valuable than creating a complex world scenario.

Some interesting insights / possible directions of project development:

- thinking about every single person as an individual palette of colors (pigments)
- thinking about the scent of man as the output of what we eat
- thinking about the unnaturality of make-up
- thinking about makeup as additional layer convering the naturality
- thinking about impact of cosmetic industries on what we percive "natural"
- thinking about impact that beauty industry has on children or teenagers

I think that this week was a collection of different thoughts, ideas and insights for me, which I still find difficult to combine into one reflection, especially in the context of my own interest. The anxiety of choosing the topic, mentioned in last week's submission, is only getting stronger.


____
##Stories. Floating questions

So what is history really? What elements do we use to tell one? What do we want to talk about, what do we want to hear about? Or perhaps it's more important for designers to listen to things that don't want to be said? Watch performances that don't want to be shown?


____
##Playlist

<iframe width="560" height="315" src="https://www.youtube.com/embed/videoseries?list=PL6gbMH_ViGtlItGaH6lJNw0Nc9yK54mvV" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
