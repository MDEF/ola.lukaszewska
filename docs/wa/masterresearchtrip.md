#Make Here research reflections

_____
##Make Here BCN

![](assets/MAKEHERE.svg)


Make Here is a project to create a community around local production. We want to trigger the conversation between makers to foment local production by sharing knowledge, information, ideas, recommendations etc. in a city or neighbourhood. It's a platform connecting people wanting to make to people already making.

![](assets/Liken-profile.jpg)

Make Here started as a one-week project and was inspired by Make Works and Fab City. We initially mapped information about maker-spaces, material suppliers, workshops and other creative spaces in Poblenou, Barcelona.

More on the journey, process and the future plans can be read under following links:

[**| PROCESS**](https://mdef.gitlab.io/ola.lukaszewska/wa/makehereprocess/)
[**| PLATFORM**](https://mdef.gitlab.io/ola.lukaszewska/wa/makehereplatform/)
[**| TOOLKIT**](https://issuu.com/olaukaszewska/docs/make-here-toolkit)
[**| MAKE HERE WARSAW**](https://mdef.gitlab.io/ola.lukaszewska/wa/makeherewawa/)


![](assets/mdef-map2.jpg)

_____
##Reflections

<iframe src="https://player.vimeo.com/video/313129045" width="640" height="360" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
<p><a href="https://vimeo.com/313129045">Make Here BCN</a> from <a href="https://vimeo.com/user94271357">Make Here</a> on <a href="https://vimeo.com">Vimeo</a>.</p>

The week-long Make Here project we've been working on in Barcelona was a perfect example of measuring up and eating presumably as much as we could bite - the time constraint allowed (or maybe rather motivated?), in this case, to plan each day, pushed to write down a very detailed list of tasks divided into roles and eventually setting the final goal - even if the "final" project was a kind of the first draft without critical filters, it was also a stage from which the whole team was satisfied and it became a milestone. Such a methodology, although for some it may seem obvious, I will definitely use in my work in future semesters - dividing the final project into smaller units, both time- and task-wise. This, of course, helps to plan the work, plan the margin of error and, to some extent, control the process, but for me it primarily removes the feeling of being overwhelmed, which, as shown by Make Here, greatly affects productivity.

What else did this project teach me? Discovering so many places, workshops, studios, and spaces in such a small area of ​​Poblenou brought similar reflections to the trip and "trash hunting" from the Bootcamp Week of the course - this place contains many hidden gems. There is a whole community of creators who are often willing to share their knowledge (such as Biciclot who teaches children and youth to repair bikes, I love it) or resources. But this community, I got the impression, is a bit hidden and doesn't work in a connected network, is not communicated, i.e. if someone outside of Barcelona, ​​not knowing the community, not knowing the city, arrives to Barcelona and happens to be a maker who wants to do something (and obviously doesn't belong to FabLab), I imagine it wouldn't be easy to find such creators or resources at all. You can, of course, try your luck and just hang around the city, but after all it's still usually so that the adventure begins with Goggle. And really, there is no marketing but the word of mouth that has a huge message power. In any case, we would not be able to look at some of these places, if not for a word from another of the makers. Make Here was supposed to be just that - an online grapevine, to which anyone can add something and contribute. Why is this observation important to me? Because it may turn out that there are so many more specialists in my bio-theme around me than I am able to search on the internet or better yet, maybe there is already a community that works communally on similar areas and from which, hopefully, I will be able to learn a lot or use their work milestones as my starting point and bounce with the topic in my own way.


![](assets/makeherebcn-2.jpg)

________________

What does the toothbrush tip and  creating changes or intervention points have in common? See the video below to understand. And apart from the obvious reflection that small steps can make huge differences, there is one more aspect that cannot be underestimated. Community.


<iframe width="560" height="315" src="https://www.youtube.com/embed/xd_BVg-O69I?controls=0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


Presious Plastic is an initiative launched by Dave Hakkens in 2013 as his diploma project. His activities aimed at accelerating the plastic revolution and dealing with overflooding plastic waste. Presious Plastic provides free knowledge, tools and solutions for recycling plastic. These actions, which began as graduation project thinking, has had a snowball effect ever since. Today thousands of people take part in this global action - they build machines, contribute to them, make their own new products or create art. It's just enough to look at the map to visualize this phenomenon.


<div class="map-embed w-embed w-iframe"><iframe width="560" height="315" src="https://map.preciousplastic.com/?menu=false&filters=WORKSHOP&scrollzoom=false"></iframe></div>


Our Make Here also bases its activity on the community. On the community of people who make; on the community of people who know who, what and where; on the community of people who want to learn and create, on those who are searching. I think that all of us, working on this project, hope that it will gain momentum, expand, spread to other cities (which I would like to initiate in Make Here Wawa in Warsaw, Poland, more on that I write [here](https://mdef.gitlab.io/ola.lukaszewska/wa/makeherewawa/)). I'd really wish both Make Here and my master project to make the most of possible resources in order to create the shift. But the moral is also one more and I would like this idea to accompany me at work, especially in the moments in which I think, "what is it for actually?" - locally created can be a global opportunity.

A drop in the ocean. But the ocean is made out of millions drops.
