#Plotter

Plotter is a machine that prints (or cuts) vector graphics continuous point-to-point lines directly from vector graphics files or commands. The blade (knife) moves in X axis while the rollers move the sheet of material in Y axis. It can cut vinyl, paper, cardstock, heat transfers and even some fabrics.




##Start

![](assets/plottersticker.JPG)

I started the learning process from being instructed by a colleague and cutting out one circle. Once I was (barerly) sure I can learn on my own and try one of my graphics, I prepared curves to be cut (note: remember to join the curves that can be joined), loaded material and started cutting, trying different sizes of the graphic, learning from mistakes and finally screen printing (more in the next chapter).


![](assets/ploter01.jpg)


![](assets/cutting.gif)


![](assets/ploter004.jpg)


![](assets/ploter005.jpg)


![](assets/ploter006.jpg)




##Screen printing with vinyl stencil

For screen printing, I measured the size of available silk screen, made a frame around my design in order to have a negative stencil and once it was cut, I placed it on the screen, applied paint and printed on both textile and paper.


![](assets/ploter003.jpg)


![](assets/ploter002.jpg)


![](assets/ploter001.jpg)




##Effects


![](assets/ploter02.jpg)


![](assets/ploter03.jpg)




##Mistakes

![](assets/mistakes.jpg)

Along the way of this rather brief experience I was able to make couples of mistakes. Here is a short list of them:

- Files: some of the first graphics I prepared haven't been read by the plotter software - I've tried different extensions, like .svg, .ai, .dxf or .dwg. At the end .dwg was the most suitable one. The thing I forgot about was to set up the right color of lines - that matter is quite similar to laser cutter, where different preferences (if it should be either cutting or just pressing in order to be able to fold etc.) can be assigned to different colors of the curves.

- If the rollers in the plotter are not locked, the paper will start to rotate and therefore the cutting or folding won't be done perfectly.

- While cutting relatively complicated and detailed cuttings, it's good to use the adhesive mat while cutting (default mat included in the plotter kit) or use transfer foil to be able to first apply the whole sheet and then take off the unnecessary part.

- Once again the rule "measure twice cut once" applied - in this case it was not cutting deep enough and letting the knife slide two times over the same curve. The plotter is quite a precise machine (in a sense) but not necessarily twice on the same material - it misplaced the cuts (the material has been moved).

- Screen printing with the vinyl instead of blocking emulsion doesn't make the stencil impermeable to the ink so the printouts are not super precise. The vinyl doesn't stay glued to the screen, but that might be as well the matter of material or screen and then also the paint I used. It would require more tests and tries but anyway it's quite a playful and fast way of making stencils and printing things.
