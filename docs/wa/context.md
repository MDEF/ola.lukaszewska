#Context

![](assets/context.JPG)

______________
##Area of Interest

My area of interest, defined and refocused during Term I and further represented in the intervention topic, starts from something as simple as it is complex - how children perceive and explore the world around them and it follows by their ability and stubbornness in asking the ground question: why. I am exploring the fields of biodesign in a context of communicating the science and design (together) for children's culture. The interest involves creative teaching and learning methods, especially hands-on experience. **Additionally, during the whole process I want to keep in mind that I don't do that for the sake of education, but for the sake of future creative minds, adding my idea to the whole pool, for the sake of nature because from how children understand the world around them comes how they will understand and respect the environment, planet, understand the importance of nature and the damages we have done** ZMIENIC - I obviously know that I am not changing it just with my own contribution, but the stock of projects heading in that direction, trying to create a mindset shift - as the state of the art shows - is rich. I want to contribute to the approach (or rather shifting the thinking into direction) of not wanting to protect the environment but creating a world that doesn't need protection or restoration.

I am dividing my intervention, at this stage of the project, into three main topics that will be the themes of kits / workshops:

**LIVING LIGHTNING** - working with bioluminescence organisms, bacteria and algae and interspersing them into bio-engineered objects of everyday use, teaching about the organism itself, about alternatives of known production methods, on a human scale, introducing more holistic thinking about life of an object.

**LIVING COLORS** - working with actinobacteria that have ability to dye, teaching to use biology as a medium in order to create a colorful fabri (or other materials) as an alternative to pigments / dyes in the industry, challenging the idea of beauty and aesthetics, teaching about environmental impact.

**BIOPHOTOVOLTANIC** - working with the process of electrons produced during the oxidation of water by photosynthesis, showing the potential to alternatively produced energy coming from plants, teaching about simple circuits and biological processes.


_________________
##Description of the Intervention

*Our education systems are stuck in the past. It’s time to shake up the system with a bit of design and innovation, and build skills relevant to the 21st century. Public education was created back when the interest was on building skills for the 19th century: the industrial revolution. While this system includes skills that are still relevant, it’s no longer enough. Children’s doctor Dr Remo Largo puts it perfectly: The children are living in the 21st century, parents and teachers are in the 20th century, and the education system is stuck in the 19th century.*

[(quote source)](https://ixds.com/bringing-learning-into-the-21st-century)

UNESCO identifies 21th century skills as, to name few, critical thinking, problem-solving approach, team-work, creative thinking and communication. Nowadays school students' mindsets are theoretically formed, being used to learning by hearth, memorizing and preparing for tests and exams, where at the end there is either good or bad answer. It's quite binary whereas the learning experience should be focused on empowering students abilities instead of pointing out the weaknesses. And even though some schools are adopting the STEAM - Science, Technology, Engineering, Art and Math - approach, the problems stays, as the system as whole is generally restricted.

My intervention aims at this gap. By **including biodesign in children's education**, I want to introduce cross-curriculary possibilities.  Multidisciplinary approach is crucial in order to face different issues we are already dealing with, starting from environmental crisis through lack of equality to exclusion. Today, the rift between disciplines is narrowing by necessity so I believe that by widening student's skillset we can give them tools to learn. And because kids learn through hands-on experience, experimenting, cognitive process introduced in a playful way, they are able to explore their own natural, "built-in", curiosity, which should be the strongest tool to be used.

"Biodesign" or biological literacy is one of the emerging fields of (synthetic) biology by design. It is not (not only, at least) about shaping biology to fit humans needs, but doing so more in tune with biological systems, with the nature and environment. Such intervention, of course, has its own risk, because after all you interfere with nature, but the possible advantages outweigh the risks because of the urgent need to reform our todays actions. Biodesign teaches problem-solving approach, multidisciplinary work and critical thinking while sneaking in the biology knowledge.

The nature of the intervention is prescriptive, as the first iteration of the project would be a do-it-yourself kit allowing a child create its own first bio-object by following the instruction in a teacherless project. Possible workshops would also have a step-by-step instruction with a supervisor. Next iterations of the project would include educating the educators instead of addressing children directly. The intervention is hands on experience, allowing the students to experience the process, be part of the creation and by playing and solving particular challenge learn the outcomes. It's also generative process provoking social and educational change.

Areas of intervention are as follow: schooling systems, schooling methods, hands-on learning, biodesign, children's culture, biology literacy, critical thinking, creative process. The intervention includes such actors as student (participants), schools, educational institutions, parents, teachers, biology teachers in particular, headmasters, FabKids (possible support), scientists (possible support) and designers.

_________________
##State of the Art

*Here are listed some main undertaking that shows the depth of the pool that is a rescource for my intervention.*

1. Schools, learning centres, institutions

**FABKIDS** - The Fab Kids, is a creative laboratory that favors the development of intelligence, creativity and imagination of children and youth. It is a place where thinking is encouraged and innovation occurs, a space where educational and recreational activities take place, focused on design and digital fabrication.

**OPEN WETLAB** - The Open WetLab is a leading place for bio-art, biodesign and do-it-together biology in the heart of Amsterdam. It works together with artists, designers, scientists, and hackers at public participation projects. The Lab regularly organizes well-visited meet-ups. Beside that, artists and students can work in the lab as artistic researcher-in-residence and new biohackers are trained at the BioHack Academy: an international highly acclaimed course for biohackers, initiated by Pieter van Boheemen. The BioHack Academy learns individuals to build their own lab, create bio experiments, and teaches the basics of synthetic biology and critical reflection.

**EXPLORATORIUM** - The Exploratorium isn't just a museum; it's an ongoing exploration of science, art and human perception—a vast collection of online experiences that feed child curiosity. A visitor can step inside a tornado, turn upside down in a curved mirror, dance on a fog bridge, and explore more than 650 hands-on exhibits.

**COPERNICUS SCIENCE CENTER** - Copernicus Science Centre is a science museum. It contains over 450 interactive exhibits that enable visitors to single-handedly carry out experiments and discover the laws of science for themselves.

**BIO BULDERS LAB** - The need for STEM education in today’s educational settings is as great as the need to support its ongoing growth. To that end, BioBuilder takes a comprehensive approach to the emerging field of synthetic biology, providing exceptional programming available for students and educators alike. For students, there is the chance to integrate biology and engineering through practical, hands-on lessons and club activities. For educators, there is the vital opportunity for relevant professional development that fosters new methods of teaching designed to engage and inspire the young scientists in their classrooms. BioBuilder’s Learning Lab fosters innovation by welcoming students, teachers and community members into LabCentral’s entrepreneurial environment. The Learning Lab is a fully equipped teaching space that provides local innovators a welcoming environment for hands-on engagement with BioBuilder’s programming.

**BIOACADEMY** - Bio Academy is a distributed educational model providing a unique educational experience where each node that participates in the program is part of a the global Fab Lab network. It is a computational and synthetic biology program aiming to guide students into using engineering principles to design and assemble biological components in the context of small and networked labs.

2. Creative sets w robotics and programming


**KIWI CO** Built out of a belief in the importance of developing creative confidence and that this confidence helps kids think big and act like creators and producers instead of just consumers. Kids with creative confidence don’t assume one "right way" to build with blocks, paint a picture, or solve a problem. Their unique way is the right way. The sets are divided into age ranges and cover different topics - science, engineering, craft in a system of monthly subscription. The sets contains instructions for objectives but also further steps for parents - what else can be done with the elements in the set.

![](assets/kiwico.jpg)

**ARDUINO** Arduino is on a mission to make technology accessible to everyone, and into the hands of every student and educator. To make this possible, we have created Arduino Education: a dedicated global team formed by education experts, content developers, engineers, and interaction designers. Arduino Education is focused on creating the next generation of STEAM programs — integrating Science, Technology, Engineering, Arts and Math — while supporting the needs of teachers and students throughout the educational journey. Arduino set contains basic elements for first projects and Arduino platform is a website with further learning content.

![](assets/arduinokit.jpg)

**LEGO** makes programming and building own robot fun and playful, gives rebuilding possibilities. LEGO Mindstorm combines physical and digital learning experience, it's hands-on experience in the STEAM education approach. Their goal is to inspire and develop the builders of tomorrow. The set contains: LEGO bricks, motors, sensors, programable brick, programming learning platform. It is believed that play is essential because  when children play, they learn.

![](assets/mindstorm.jpg)


3. Learning/teaching biology

**AMINO LABS** At Amino Labs, the goal is to make genetic engineering accessible to everyone. This will enable the next industrial revolution of personalized manufacturing using biology and help solve the world's biggest problem from fuel to food to medicine. At the moment, genetic engineering affects more than a billion people every day, is a core part of our society, and yet only a small fraction of the population understand it. They propose the kit for learning biology engineering for schools and homes with the tools and learning platform.

![](assets/aminobio.jpg)

**BENTO BIO BOX** mobile genetic setup, contains PCR machine, microcentrifuge, transiluminator, gel electrophoresis. Allows the user to work independently on the place, making the tests more accessible and not dependent on the lab.

![](assets/bento.jpg)


4. Biodesign


*ecological object engineering - replacing industrial and mechanical processes*

**ALGARIUM** can simple algae bring both biofuel production and colorful design into our homes? generating living light as an ever-changing decorative element can also serve as a resource of green energy

![](assets/algarium.jpg)

**BIO-LIGHT** glass chambers filled with either bioluminescecnt bacteria and nutrients or the enzyme and proteins neede to sustain illumination - with bio digestion at its cluter, can the home become more self-sufficient and less wastefull of natural resources?

![](assets/biolight.jpg)

**ECOVATIVE** are the mushroom the answer when it comes to finding a versatile and sustainable alternative to plastic?

![](assets/ecovative.jpg)

**BIOCOUTURE** bacteria-grown clothes with reduced environment impact recast a fashion statement as an ecological stance?

![](assets/biofashion.jpg)

**MOSS TABLE** instead of treating it as a troublesome weed, should we encourage moss as a renewable source of energy? potential to provide cheap electricity may cast them in light; how moss can be use to power small electronic devices? (biophotovoltaics)

![](assets/moss.jpg)

**BIOLUMINESCENT DEVICES** living lightninng that can be configured for different public spaces - algae, bacteria, organic growing devices that requires no metals, indusstrial manufacture tec. integrting a natural, chemical reaction generatied by an organisms into the built environment serves to challenge the conventional divide between fabricated and natural spaces while adressing the urgent need to increase achitecture's ecological performance.

![](assets/biolight2.jpg)


*experimental funcition - speculative objects, teaching tools, provocations*

**INTERWOVEN AND HARVEST** DEMONSTRATING THE POTENTIAL TO TRANFORM nature-based or geometric forms into actual living designers

![](assets/woven.jpg)

**SYMBIOSIS** living letters that grow, change collor and eventually die - could this be a future of living, dynamic graphic design? respond to pullution by printed media; takes radical approach 0 utilizing bacteria to grow letters

![](assets/abema.jpg)

**BACTERIOOPTICA** a living chandelier that allows the owner to experiment with the lightning effects offered by different bacteria

![](assets/optica.jpg)

5. Biohacking movement

![](assets/biohackacad.jpg)
