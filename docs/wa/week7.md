#**WEEK 07. The way things work**
An introduction to physical computing by hacking everyday objects
*by Guillem Campodron, Victor Barberan, Oscar Gonzalez*

___
##Deconstruction

We are surrounded by the endless sea of ​​objects, devices and machines. We interact with them, even create a relationship with them. But we often get rid of these objects for a very simple reason - they stopped working, they ceased to fulfill their functions. Quite recently, during the conversation with my father, the topic came down to disappearing jobs (as in for example craftsmanships) and places connected to them. He told me that in our hometown there was once a place where the owner would repair broken umbrellas. At that particular moment it seemed rather surrealistic, but some time later it occurred to me that just like a shoemaker or car mechanic, such a place has a complete sense of being and existing. But at such a terrible pace of growing consumerism in which we are being wrapped up recently, we stopped thinking about the possibility of repairing our equipment and got used to the process of quick replacement (however I have the impression that there are outbreaks in which the trend of repairing and exchanging rather than buying is growing).

Why?
At some point this is due to the simple fact of our everyday items being designed in such a way that their repair is (almost) impossible or very expensive (halo, Apple customers?). It's very easy to verify that by trying to dismantle some equipment from our everyday space, especially the one containing electronics. Because it's not just about the welded plastic packaging that needs to be broken, but about software that is not "hackable".


____
##What we found

What do the objects hide inside? During the classes were introduced to the task: disassemble, distyle and resynthesise the object in order to see what uncover mysteries are hidden inside an often idealized shell.

We fought against the EPSON scanner / printer. We have deconstructed it completely, in the meantime during the whole we were also trying to understand its both mechanical and electrical mode of operation, the relationships between the given integrated circuits as well as between individual systems.

![](assets/deconstruction.gif)

Here are some of the things we have found:

- light sensors
- USB readers
- RAM
- motors / rotors
- systems controlling the level of ink


____
##Reflections on deconstruction

![](assets/hack.jpg)

In addition to the unexpected beauty of sponges that absorb the ink (in the pictures above in pink), when disassembling the equipment, some reflections occurred to me or where discussed during teamwork. I summed those up in following sub-points.

**REPARABILITY**
Why aren't things designed to be reparable? As stated in Design of Everyday Things, *Designing well is not easy. The manufacturer wants something that can be produced economically. The store wants something that will be attractive to its customers. The purchaser has several demands. In the store, the purchaser focuses on price and appearance, will pay more attention to functionality and usability. The repair service cares about maintainability: how easy is the device to take apart, diagnose, and service? The needs of those concerned are different and often conflict*.

I do not want to talk about the history of "light bulb conspiracy" and purposeful aging of products (the idea is to program - for example - the washing machine in such a way that after a given amount of washing the washing machine will "break" and will not be repairable, which will force the user to buy a new one) but about the desirability of design without the possibility of repair - for instance, plastic shells that are welded. I wondered, while working with the printer, how few things are contained in the user manual - it talks about how to connect, configure and use an item, sometimes it describes the most common errors. But I don't know if I have ever seen an instruction that would allow the actual "DIY" kind of repair.

Of course, in these reflections, there can not be no "open source" projects, which somehow go against the above-mentioned products. "Open-source" is public, open information, data, "insights", initially about software, today covering basically everything - hardware blueprints, biohacking, literature, art and even politics. They aim to strengthen the future in which systems are shared, open, sources are known, and objects are hackable. One of the projects that I found during pre-submission researching & reading is Open Source Ecology project. I was interested mainly because of the scale in which it operates - it says about the most popular everyday objects, but you can find there a recipe for building your own tractor. Dear farmers, what do you think about that?

*This work of distributing raw productive power to people is not only a means to solving wicked problems – but a means for humans themselves to evolve. The creation of a new world depends on expansion of human consciousness and personal evolution (...) and to become responsible for the world around them.* To find out more about Open Source Ecology project click on the photo.

[![](assets/gvcs.jpg)](https://www.opensourceecology.org/about-overview/)


**HACKABILITY**
The classic definition of "hacking" according to the [Cambridge Dictionary](https://dictionary.cambridge.org/dictionary/english/hacking) is - "the activity of illegally using a computer to access information stored on another computer system or to spread a computer virus". When we think about the "hackability" of home appliances, we're obviously not talking about virus poisoning but about the possibility of reusing programmable platforms from devices found in equipment. As we learned during the classes, many of these platforms are impossible to hack - they are produced / programmed in this particular way. You can search for the type / model of platform and specific firming of a given company, such as EPSON, you can also find information that actually one or two people hacked such a plate, which obviously draws attention to the advancement of such a process. If the plate is impossible to reprogram, it becomes "garbage". It's an ugly word to describe something that could serve to create new opportunities in the sense of spreading knowledge and awareness about the availability of such a process no further than within the reach of the home.

Many instructions on many different devices may be found. I'll note the library of such pages at the end of the entry as "references", but the instruction itself is not enough. You also need to have quite a large amount of knowledge to not only "get on" for hacking, but to know how and with what to combine platform data, because not all of them will have such a rich description, instructions and "DIY", such as Arduino, which brings me to the next question.


**KNOWLEDGE ABOUT SOFTWARE AND ITS DISTRIBUTION**
"He who seeks finds it," says a wise proverb. It's just that to start looking at all, you need to know what. And maybe even - you have to know that you can. And it seems to me that this is in a sense the whole problem. Many devices include programmable platforms, motors, sensors. But we don't always realize it. And the knowledge that's needed to use the part later - even to learn - is available, it is a fact, but among people there is no real awareness that you can reach it. Perhaps the distribution of the knowledge is an issue that needs to be resolved.


____
##Project. Task description
*team: Barbara Drozdek, Julia Quiroga, Silvia Matilde Ferrari Boneschi, Nhu Tram Veronica Tran, Gabriela Martinez Pinheiro*

![](assets/IO.jpg)

We focused on the output. We wanted to intervene in the space, enter the "dialogue" with the recipient, get bystander's attention. We wanted to play with light and an object that could give a large, varied effect. Big Bang is a blowing and exploding machine for balloons filled with confetti, which warns with color and light intensity.

____
##Construction

To assemble the machine we needed the following elements:

![](assets/arduinoset-01.jpg)


Here is some sneak peaks from the programming, assembly and construction mounting process.

![](assets/construction.gif)


____
##Exhibition

Final construction. It's playful and engaging enough to be posted on Instagram right away. Video by [Ryota Kamio](https://www.facebook.com/ryota.kamio1).

<div style="width:100%;height:0px;position:relative;padding-bottom:176.667%;"><iframe src="https://streamable.com/s/k4x3v/mgoddj" frameborder="0" width="100%" height="100%" allowfullscreen style="width:100%;height:100%;position:absolute;left:0px;top:0px;overflow:hidden;"></iframe></div>


____
##Why to build useless machines

Machines built this week in the sense of use were, well, useless. But also allowed me (us?) understand what a computer is, what is the difference between direct current and alternating current, what does "compilation" means and how to assemble an Arduino set to transform into a working machine. And they were simply integrating the space and the audience.

I want to finish this week's reflection with honourable mention of my favourite robot-focused channel on Earth (but actually it's on Youtube). So now, without further ado, here is The Queen of Shitty Robots, Simone Giertz.

<iframe width="956" height="538" src="https://www.youtube.com/embed/OOGeSmzWktA" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


___
##References

Most of the references come from sources shared by tutors during workshop.

- [**HACKADAY**](https://hackaday.com/)
- [**LOW TECH MAGAZINE**](https://www.lowtechmagazine.com/)
- [**FAB ACADEMY**](http://archive.fabacademy.org/)
- [**ADAFRUIT**](https://learn.adafruit.com/)
- [**INSTRUCTABLES**](https://www.instructables.com/)
- [**FORBOT PL**](https://forbot.pl/blog/)
- [**PROCESSING**](https://processing.org/)
- [**BOTLAND PL**](https://botland.com.pl/pl/content/188-spis-tresci-poradnikow)


![](assets/books.JPG)
