# WEEK 01 - project management


##Managing projects - software



##Creating FabAcademy subpages

To enter new cards and subpages, I had to edit the "mkdocs.yml" file, which contains information and addresses of (sub)pages. At the beginning, this operation didn't work for me because I had a duplicate file in my folder. After cleaning the path, I could start adding new tabs.


![](assets/fablab01.JPG)


This is the structure of the "Term 1" page and the construction of this file in general. By copying such a structure, I could have similarly styled new cards for the 2nd trimester and a new page for Fab Academy, with later division into weeks and discussed topics.


![](assets/fablab02.JPG)


The next step was "copying" this structure and rewriting it for the wanted subpages. First enter the name (title) to be visible on the page and then after the colon name of the .md file, i.e. the file that contains the content, gives the page - *Title to be shown on website: wa/filename.md*


![](assets/fablab03.JPG)


Then I had to actually make the files of the pages in which I can later enter the content. To do this, create a new file (Ctrl + N), save it (Ctrl + S) in the appropriate folder (in the path that we declared in the previous step) and give it a name with the extension (again, the name that was declared in the previous step).


![](assets/fablab04.JPG)


Now I only had to save, commit changes and finally click "push" to apply new parts to the website.

Such operations, which I described above, didn't work for me - I couldn't see all of wished tabs on the website, therefore I assumed not all subpages were created properly. I noticed that one of probable mistakes (I think, though I do not know for sure) was to broadcast files for titles that were just a kind of menu to develop, under which the right pages would be hidden, like "Term 1". The changes are visible on the screenshot attached below.


![](assets/fablab05.JPG)


What finally worked was the arrangement of spaces, empty spaces (margins?). I copied them from the first trimester and so the menu was completed.


![](assets/fablab06.JPG)


It was not the only problem. Although the menu looked as if the changes in the file worked, something was still wrong. After clicking on the subpage, such a view was shown.


![](assets/fablab07.JPG)


As I was pointed out by my colleague, the error was in a way really trivial. The file path wasn't correct - I just provided the name of the .md, not the folder in which the file is located.


![](assets/fablab08.JPG)
