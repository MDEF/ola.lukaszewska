##**WEEK03. Design for real digital world**
*by Ingi Freyr Guðjónsson and Francesco Zonca*


Team: Thomas Barnes, Maite Villar Latasa, Rutvij Pathak, Ryota Kamio, Vasiliki Simitopoulou, Vesa Gashi & myself


We live in the world of consumption and fast disposal. Do we think about where the things come from and where do they end up? What are the consequences of their production? What is waste? How do we see production and reproduction?

#Trash hunting
Barcelona Trash Days are days during which in particular districts it’s allowed to dispose trash by simply putting on the streets or by trashbins. Every district has its own characteristic trash and collecting those is an experience on its own. Poble Nou means collecting tons of industrial wastes and materials from the streets. This week goal was to use those abandoned items and in a way rescue them, giving them new life and let them serve as our classroom furniture. We hacked the things to create new objects, explored possibilities of manufacturing in FabLab and evidently learned about team work.

#Groups and brainstorming
After being divided in groups (to have different skills in the team, including communication tools, workshop experience, 3D modeling etc.) we decided to use brainstorming as a tool for creating quick ideas on how and by what can we change the classroom. After that, we sum everything up and we formulated groups of needs. Next step was to give our thoughts the form and that being said, we created sketches and simple digital prototypes.

Key features of clusters are presented below.

![](assets/DFTRDW01.jpg)

#Proposal
Our main approach was “bottom up” design (there are plenty of plain boards available from the scraps on the streets, so it was appropriate to start to think what kind of material do we deal with and how can we use its properties) although it couldn’t be the only one, as we had to have in mind what the classroom needs. Among options proposed by the team, The Maker Space was chosen. The goal and focus was to create a working tables for small jobs such as soldering or cutting with extra features, including storage of materials. Important part of the project was the distribution of existing elements in the classroom, such as projector, tables and chairs.


The whole presentation is available here:
https://issuu.com/olalukaszewska/docs/design_for_the_real_world__day1_2f2


![](assets/DFTRDW01.jpg)

![](assets/DFTRDW02.jpg)

![](assets/DFTRDW03.jpg)

![](assets/DFTRDW04.jpg)



#Process

![](assets/steps01-01.jpg)


Steps of our process were as follow (shown above): brainstorming, ideation, digitalization, fabrication, presenting. But that is a simplification, as the path (shown below) was more complicated and not linear. Every step required at some point coming back or skipping the rest. Some parts could be done without digitalization, some even with digital version and tons of measurements needed to be repaired “by hand”.


![](assets/steps02-02.jpg)


Digitalization was a process of translating our sketches into real-dimension models and preparing them for further fabrication - coming from digital to physical world by using CNC machines and electric tools. After few tries in assembling, the basic concept was ready to present. Final table will be finished upon upcoming week.

![](assets/DSC_0540.jpg)

![](assets/DSC_0424.jpg)

![](assets/DSC_0484.jpg)

![](assets/DCS_03.jpg)

![](assets/DSC_0541.jpg)



#Parametrization

As mentioned before, one of the scraps that are easy to find are plain boards of different dimensions. But they are not always the same. Therefore we had an idea of creating a parametric representation of our desk for the future us. That will help creating a model for preview with a given dimensions and a file ready for CNC machine.

![](assets/tablespreview.jpg)

![](assets/tablespreview_perspective.jpg)

![](assets/scriptpreview02_small.png)

![](assets/scriptpreview_small.png)




*icons list:
ideation cycle by Weltenraser from the Noun Project
sketch by Mark Claus from the Noun Project
Laptop by Nattapol Kuepornkiettikul from the Noun Project
CNC Router by Nick Green from the Noun Project
Desk by Creative Stall from the Noun Project*

photos:
Ryota Kamio, Aleksandra Łukaszewska
