#PCB

**What's a PCB?**
A printed circuit board (PCB) mechanically supports and electrically connects electronic components or electrical components using conductive tracks, pads and other features etched from one or more sheet layers of copper laminated onto and/or between sheet layers of a non-conductive substrate. Components are generally soldered onto the PCB to both electrically connect and mechanically fasten them to it.



The Electronics Production assignment is to mill the board, stuff it with components and program it

**Tutorial** http://fabacademy.org/2018/docs/FabAcademy-Tutorials/week4_electronic_production/fabisp.html


Board traces:
![](assets/traces.png)

Board outline:
![](assets/interior.png)

 FabISP labeled board diagram (to know what components are needed):

![](assets/components.png)

Components list:

- 1 ATTiny 44 microcontroller
- 1 Capacitor 1uF
- 2 Capacitor 10 pF
- 2 Resistor 100 ohm
- 1 Resistor 499 ohm
- 1 Resistor 1K ohm
- 1 Resistor 10K
- one 6 pin header
- 1 USB connector
- 2 jumpers (0 ohm resistors)
- 1 Crystal 20MHz
- two Zener Diode 3.3 V
- one USB mini cable
- one ribbon cable
- two 6 pin connectors


**Soldering**

![](assets/DSC02675.JPG)

![](assets/DSC02679.JPG)


**Board**

![](assets/DSC02684.JPG)
