# 3D Design

##Fusion 360

##123D Make

![](assets/123D01.JPG)
![](assets/123D02.JPG)
![](assets/123D03.JPG)
![](assets/123D04.JPG)
![](assets/123D05.JPG)



## Grasshopper

Here is one of the project I took part in during parametric workshop. We used Grasshopper's plug in Kangaroo to fid a form by mimicking gravity forces by defining anchor points for the structure. After being glad with the general form, panelised it, then created ribbing, points for connecting parts (panels) with zip locks and at the end number it so we could now which panels go where and rescaled it and projected it into planar that was our available sheets of material to work with, so we could have a ready CNC file. It sounds quite simple and linear, but the whole group together with tutors spend almost full 72 hours with rather quick breaks and litres of coffee, going back and forth few times to actually develop the script and files, living small amount of time to eventually build a structure.

![](assets/parametricfab01.JPG)
![](assets/parametricfab02.JPG)
![](assets/parametricfab03.jpg)

I find Grasshopper to be quite an useful tool - from repeating simple shapes, through data visualisation and form finding to constructions, optimisation and environmental analysis.

##Blender

##?
