#Invention, intellectual property, income


__________
##Types of licences

While making things, there are different ways of sharing, licensing and patenting ideas, which are important to know for both side - the user and the maker (creator, owner etc.) The general overlook is presented at this graphic:

![](assets/rights.png)

 FOSS stands for “Free, Open Source Software.” The rights can range so much as to be completely relinquished to totally retained.


**Copyright** is a form of intellectual property that grants the creator of an original creative work an exclusive legal right to determine whether and under what conditions this original work may be copied and used by others.


**TYPES OF LICENCING**

- **public domain** refers to creative materials that are not protected by intellectual property laws such as copyright, trademark, or patent laws. The public owns these works, not an individual author or artist. Anyone can use a public domain work without obtaining permission, but no one can ever own it. Example: work of Bach or Beethoven; [(*source*)](https://fairuse.stanford.edu/overview/public-domain/welcome/)

- **copyleft**, distinguished from copyright, is the practice of offering people the right to freely distribute copies and modified versions of a work with the stipulation that the same rights be preserved in derivative works created later. Example: Linux, Mozilla Firefox; [(*source*)](https://en.wikipedia.org/wiki/Copyleft)

- **permissive licences**  is a free software licence for a copyrighted work that offers freedoms such as publishing a work to the public domain. In contrast, copyleft licences like the GNU General Public License require copies and derivatives of the source code to be made available on terms not more restrictive than those of the original licence. Example: MIT License; [(*source*)](https://simple.wikipedia.org/wiki/Permissive_software_license)

- **non-protective licences** is simply a non-copyleft open source license — one that guarantees the freedoms to use, modify, and redistribute, but that permits proprietary derivative works. So non-protective FOSS establishes copyright, but leave you free to do as you will with the code, including modifying it.  The only gotcha has to do with redistributing; if you want to redistribute, you must use the same license. Example: [(*source*)](https://www.monitis.com/blog/be-a-savvy-consumer-software-licenses-explained/)

- **protective FOSS licenses** come with the aforementioned copyleft restriction. With a protective FOSS license, you can still do as you please, but you must attach the same license to whatever results.  In other words, you cannot go making the result proprietary, or selling it, should the license prevent selling it. If you find yourself dealing with protective FOSS, you definitely want to dive into the license in detail, because that license will govern everything you later do with it and anything derived from it. [(*source*)](https://www.monitis.com/blog/be-a-savvy-consumer-software-licenses-explained/)

- **proprietary** software is software that is owned by an individual or a company (usually the one that developed it). There are almost always major restrictions on its use, and its source code is almost always kept secret. The source code represents their intellectual property and they probably don’t choose to share it. Example: most of the video games;  [(*source*)](https://www.monitis.com/blog/be-a-savvy-consumer-software-licenses-explained/)

- **trade secret** the realm of the super-restrictive. Trade secrets fall into the realm of proprietary software, but with an additional kicker. With trade secrets, the protected software in question presents a substantial, economic/competitive advantage to the secret holder. To put a bit blithely, here we have the stuff someone will really sue you for. Example: Google trade secrets, Kentucky fried chicken, WD-40 formula; [(*source*)](https://www.monitis.com/blog/be-a-savvy-consumer-software-licenses-explained/)


__________
##Creative Common

Creative Commons (CC) is a non-profit organisation that works to make it easier to share, reuse and repurpose creative material. It provides free licences that let creators give permission in advance for certain uses of their material. It is encourage to share but by using the right license. The flowchart below helps with choosing the right one.

![](assets/creativecommons.png)

I will write about my final project in the next chapter more precisely, but - following the chart -  I am ok with other people copying and distributing my content without asking for permission every time, I am ok with changing and adopting the content by them, but I want to limit how others can release their remixes - their new content must be available to remix on the same terms. I am also ok with other people making money out of their reuse of my content. According to the flowchart, I should use **Attribution-Share Alike licence**.

__________
##Emergent business

It is not accidently so that my licensing is similar to the one given by Arduino. They use the same license. The detail description can be found [here](https://creativecommons.org/licenses/by-sa/3.0/legalcode). Arduino is a tiny and easy to use open-source motherboard, a programmable microcontroller. The creators assumed that one doesn’t need anyone’s permission to make something great. Arduino provides educational products, platform and services and what is more as mentioned open-source blueprints for re-creating the board. Before Arduino electronics were either hard to get or expensive and not affordable. To learn that one must have do so through academic classes or textbooks. The way of prototyping user interaction was less interactive, as there was no tool to easily and fast create a working electronic object. The most important element that enabled the impact was its accessibility, easy use, the shared knowledge and hugely expanding community but also the decision to go open-source. Arduino unlocked a movement by changing the resources (providing starter kit and compatible parts, creating a learning platform). In a way the project did question roles - with such a tool everyone can be a maker and creator and the role of teacher goes beyond what was known from schools - Massimo Banzi in his TED talk from 2012 gives an example: “Kids, like these kids, they're from Spain. They learned how to program and to make robots when they were probably, like, 11, and then they started to use Arduino to make these robots that play football. They became world champions by making an Arduino-based robot. And so when we had to make our own educational robot, we just went to them and said, you know, "You design it because you know exactly what is needed to make a great robot that excites kids." Not me. I'm an old guy. What am I supposed to excite, huh?”. As a result, the company received a massive boom of projects - both hobbyistic and professional - that include Arduino. Arduino is open-sourcing unlimited imagination of its user and is great in doing so.

This week of FabAcademy binds with our MDEF [Design for Emergent Business](https://mdef.gitlab.io/ola.lukaszewska/wa/business/) by Ideas for Change. By describing system variables of our interventions and looking into historical system changes we were supposed to describe our own business model, supposing the project will continue after the Master.

My project proposes a starter kit for different biodesign topics (made tangible for the presentation by a "living lightning", working with bioluminescent algae) as well as (in future development) a supporting platform with tutorials and other resources. The project takes a lot from biohacker movement which in its principle is open-source, basing on global collaboration, sharing knowledge - the features I will try to implement in the development of my JAKTO LAB with those elements (mostly), making them child-friendly, parent-friendly and also useful for schools.

- equipment / tools - the DIY-BIO movement works mostly on attribution-share alike licence, letting people use the resources but asking them to share it on the same rules; a lot of "hacked" equipment, tools, labs instruction can be found, like in this [Essential Biohacker's Guide](https://www.syntechbio.com/tools); One of the exemplary elements of previously mentioned platform is the **product tab** - for instance, laminar flow cabinet. It is an enclosed bench designed to prevent contamination within the work field. Professional laboratories have specialized ones but such an object can also be hacked and built in a much simpler and cheaper version. On "my" online platform, the user can either purchase a mini home airflow cabinet if this is a more convenient version or download blueprints and instructions for making such a filter on one's own - platform with open-source user made (but proofed and checked by future team specialist) recipes, objects and solutions.

- recipes - in the spirit of DIY-BIO movement, again, I want to make the recipes that are used in the "starter kits" open and free on the website, that means not just for people who purchase the starter box, but for everyone looking - both scientific recipes and the hacked one, as the "wet" part of this project can also be adjusted.
