#**WEEK 11. From bits to atoms**
*by Santi Fuentemilla, Xavi Dominguez, Eduardo Chamorro, Esteban Gimenez*


##Bits<>atoms

If we go from bits to atoms, from digital to physical, do we reproduce, process or maybe create? What we can earn going through such a change? I don't know the general answer for this question, but this week - we could create machines doing something.

This week, besides the task of making machine, was all about getting to feel good in Fab Lab environment, learning the rules of work spaces and preparing for Fab Academy in next terms.


##A Machine That Makes Something

**CLICK ON THE GRAPHIC BELOW** to read day-to-day updated blog on our group work, pom-poms machine, description of the idea and process documentation.

[![](assets/POMPOM.png)](https://mdef.gitlab.io/dj-pom-pom/)


As the task was to be a group challenge, I first wanted to define my anchor points of interest so that I could later select the group easier and more efficiently. During the presentation of examples and inspirations by lecturers, I wrote down the following phrases:

* a machine that makes shoes (plaiting flat, folding to go to 3D)
* plaiting / weaving machine
* knitting machine
* chocolate 3D printing (becuase food)
* eggshell (as a domestic waste) material lab
* drawing machine

Later in conversation with Gabriela, we carried out brainstorming, saving all ideas for machines, products and processes, trying not to think about the (very needed) inputs yet. At that moment, our ideas could be divided into two categories: "wrapping / braiding" and "from 2D to 3D". In the meantime, we were looking for inspiration. I found an instructional video "How to make pumps" - DIY video with instructions on making pompons on two rolls of toilet paper. After (group) work on creating several quick prototypes of pompoms, I tried to translate the hand movements, understand the axes in which they operate and simplify them so that they can be transformed into a machine. Then, together with the rest of the group, we translated it into the first matrix for making pompons - a rotating platform with two posts for winding yarn. The whole process is described on our blog, linked above.

The division of works was done spontaneously among the group members. Such a natural division allowed each of us to do a job that matched the skill, i.e. which the person could perform efficiently. Taking out the lesson from the first workshop classes and keeping in mind the time constraint, at least according to my feeling, we left such assigned roles.

Together with Vicky, we were mainly working on making the matrix-box, which would hide all the electronics, enable the platform to turn in order to weave the pompoms and eventually encourage people to use it. The work involved creating the non-electrical part of the machine consisted mainly of continuous changes after testing the prototypes: file correction, laser cutting (first in the test material - cardboard, later in the main material - 3mm plywood), assembling, checking it with electronic parts (or at least the part that was available or ready at that given time), checking the matrix and making further amendments. Even the final model we presented would require a few detail changes in the future.

In the meantime, I was working on the preparation of 3D printing files - we worked on the project two weeks before Christmas, which of course required a few Christmas details - we created the relationship between the speed of rotation and the answer to the question "What is your Christmas Spirit?". For each of the answers I prepared a graphic with the particular spirit of Christmas - 3D printed buttons.

**The most important lessons learned this week:** if you work in a group, especially in different rooms, ALWAYS let the others know about any changes in the design, particularly if these changes affect the fabrication files - this will allow to avoid many unnecessary prototypes. Lesson number 2 - do not measure with a ruler. Use a vernier caliper. Especially if the dimensions are given in millimetres.


##The portfolio of fails

During this week we talked a lot about failures. This gave me food for thought - next to the regular portfolio, which is usually presented, the portfolio of failed projects should be equally important. If you think carefully, you can get quite a lessons for the future from failures and mistakes. I will surely someday prepare a summary of my nightmare projects (that I still dream about at night) with a reflection on how it affected me later or how it changed my way of working.
