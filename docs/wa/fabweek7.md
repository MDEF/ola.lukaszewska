#Computer controlled machining
*Team - click on the name to go to the website
[Vicky](https://mdef.gitlab.io/vasiliki.simitopoulou/)
[Julia](https://mdef.gitlab.io/julia.quiroga/)*

 **MAKE SOMETHING BIG**
__________________
##Introduction

This week assignment was clear: make something big! As fun as it sounds, the goal was to use and learn the basic of CAM - Computer Aided Design. It's a subtractive process, controlled usually through 3 or 4 axes, sometimes by a robotic arm (providing extra axes) and the machine we have at IAAC is Raptor X-SL 3200 1520. Depending on the material, effects and wished design, there are few main ways of milling by cutting in clockwise spin motion. I found [this](https://www.sandvik.coromant.com/en-gb/knowledge/milling/pages/up-milling-vs-down-milling.aspx) article quite explanatory for two of the main cuts, DOWN CUT and UPCUT. The difference is well shown here:

 ![](assets/upcutdowncut.jpg)


We based our design on [this](https://www.instructables.com/id/Laser-Cut-Motorcycle-Toy/) INSTRUCTABLES. To test the shape, we laser-cut prototype in a smaller scale in cardboard:

![](assets/CNC.jpg)

After everything was fitting fine in both file and the scaled model, we moved to 1:1 model. We imported it to Rhino, adjusted the size (as we had **1 plywood sheet 1250 x 2400 x 15 mm** for our disposal) and took a look at our lines. The curves are basically a path o the milling tip, so it is important to be aware of drill size we are using and the elements that will be used as joints. Another thing is to be mindful about sharp corners, insides of some cutouts etc. Here is a graphic showing some examples of what I mean (in this case photos says more). This and some other immportant insights are described [here](https://fablab.arts.ufl.edu/send-us-your-file/cnc-file-standards/).

![](assets/cncinfo0.png)

![](assets/cncinfo.jpg)

![](assets/cncinfo2.png)

Another way to deal with sharp corners is to make t-bone shape - a sort of fillet instead of 90° corner with the  Ø of the mill, so that in that specific point the machine will read to do a hole.

![](assets/cnctbone.jpg)

___________________
##Files

Eventually we had our curves sorted out so then asked FabLab staff for help with Rhino Cam, plug in for preparing files for CNC, which looked like it takes a lot of time to properly learn it and so for the few first tries help will be for sure needed. Rhino Cam allows to define the kind of cut we will be doing, sizes of drill, what tasks do we need to do (engraving - which should always go first - or cutting) and other, more advance, preferences.

![](assets/PROGRAM.PNG)

![](assets/SETUP.PNG)

When the settings were ready, we exported the GCODE. The bed of the CNC is autoamtically setup for X (0.0.0) and Y (0.0.0), but the Z axis needed to be adjusted manually by using Corner Finding Touch Plate, which let the machine calculate the distance to the board surface (show on the photo "calibrating").

![](assets/CNC-2.jpg)

Calibrating - setiing up the "0" point:

![](assets/CNC-3.jpg)

If the 0 point is done, it was time to import the GCODE (Program Tab). The GCOD will be translated to 3D representation of axes movement (the settings from Rhino CAM).

____________________
##Cutting process

When the code was prepared and CNC calibrated, we could click PLAY and start the cutting process. We had 4 steps of the process:

- engraving holes on the surface to mount the sheet of the material onto the table
- making pockets
- making the cut from the inside (that way, we firstly cut the element while everything is still attached to the board and table, instead of cutting in already cut shaped that could missplace)
- making the cut from the outside (the main cutout)

![](assets/CNC-4.jpg)

![](assets/CNC-5.jpg)

And that is why it's important to mount the sheet of material onto the table - especially big sheets can bend and move during cutting:

![](assets/CNC-6.jpg)
___________________________
##Assembling the model

Once the model was cut, we removed it from the frames (waste) and polished the edges to prepare every part.

![](assets/motor.jpg)



![](assets/cncassemble2.jpeg)

We assembled the model first with metal screws that fit our holes but then we switched to 3D printed flat joints to make the wheel spin and a "steering" handle.

<iframe src="https://giphy.com/embed/Ay0d9IB7jtrVu" width="480" height="313" frameBorder="0" class="giphy-embed" allowFullScreen></iframe><p><a href="https://giphy.com/gifs/the-dark-knight-trilogy-Ay0d9IB7jtrVu">THE ARCHETYPE</a></p>

At the end, there are few things to consider and remember (I allowed myself to copy them fromm Vicky, the co-author of this project):

- when doing the holes for the screws, the max Z for the machine should be set ridiculously high so that we avoid scratches on the board when the mill is moving from position to position

- if combining parts we should always keep in mind that the milling process may not be precise by millimeters. So the final size of the joints or the intersecting parts should be calculated accordingly. It may be needed some extra sanding or even re-cutting, if not done properly

- always remember to turn on the automatic vacuum of the raptor. It will make after-cleaning way easier

The test run showed that the model is working and it makes fun to ride it.

![](assets/cncassemble.jpeg)

DOWNLOAD THE FILES **[HERE](https://mdef.gitlab.io/vasiliki.simitopoulou/docs/_pages/fabweeks/assets/images/709.rar)**
