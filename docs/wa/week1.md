## **01. MDEF BOOTCAMP**


#**INTRODUCION: GITLAB / impression**

![](assets/GITlesson.png)

GitLab was introduced to the class as a tool for creating content - writing, posting photos etc. - for our pages. It was described as handy and useful, really ordered and easy to access. For someone not knowing programming language or operations it all seemed overwhelming and mindblowing - is there really no easier way to write a post, why Google Docs wouldn't do the job and where can I find a "GitLab for dummies" book were my first questions, but I have strong trust towards my tutors so there must be a solid reason to use that particular way of working. I keep my mind open. Now, while writing my second post, I am getting used to the interface, simple text styling commands and pushing edited posts.

![](assets/mindblowing.png)

#**CITY SAFARI TOUR**

![CITY SAFARI TOUR](assets/maps.jpg "CITY SAFARI TOUR")

Safari tour was an eye-opening experience. The only thing about Poblenou was a fact I read in a guide - it is an post-industrial disctrict. My first impression when I arrived here was that it's an dead, empty area additionally somehow seperated from the rest of Barcelona's parts. The tour has shown me that I was wrong. Poblenou is a microworld of its own, living on its own rules, actually being sligtly separated from the rest of the city. Behind almost all of those doors one can find stunning, incredible and unexpected spaces. Belowe the one we visited.

**LEKA** is a restaurant with interior designed and done by IAAC. They call themselves "open source restaurat" which at first made me feel confused, as "open source" is mostly assosiated with coding or blueprints to build up something. In case of LEKA those "somethings" appeared to be recipes for meals that are served there. You can also get schemes for furniture and layouts for their uniforms.

**INDISSOLUBLE** is an architectural studio focused on creating temporary architecture in the meaning of exhibiton stands, construction and spaces, combined with (communication?) technology - like the brain-shaped pavilion showing how does language works. Side note - someone from our group asked if they propose their clients to re-use the materials taken to create and produce the exhibitions - and sadly only few of them agreed to that. What is the "afterlife" of exhibitions?

**TRANSFOLAB** is basicaly a makers space. They called themselfes "center for trash investigation". It's one of those many hidden places in Poblenou, where it cannot be said from the outside what awaits inside. Nada, who showed us around, is an outstanding energetic person who created this space as a place for people to deal with found trash / treasures (becuase that's what can be found on the streets of Poblenou - more on that in the next chapter) and to re-use the materials by giving them new life as new fully functioning objects.

**BICICLOT**, another stop on the tour, is a shop and workshop for bikes. The team offers you the space to repair or hack the bike. They will guide you but also encourage to do it yourself. Aside from the stationary workshop Biciclot owns a mobile workshop used to create a repair station during city events, festivals but also as a educational platform for kids at schools. The best part of that place was a little kid in the back in workshop, repairing someones bike. I find it fundamentally important to educate children to repair isntead of buy new and to teach them manual skills, so this particular scene made an impression on me.

**SUPERBLOCK**, super empty? Superblock is an concept of taking the city back from cars, giving the proproty to pedestrians. meaning - of excluding from the traffic given streets taking a 3x3 blocks matrix. The idea of designing cities for humans instead of cars is noble - it gives the citizens silence, clear(er) air and safety, to name just few. But the problem is that this particular superblock done in Poblenou looks just like a empty crossing section, dangerous to stand in the middle and not parituclarly appealing. Something is missing there. It feels like an inititative that was set up, but not done with actual community using the place. Or maybe the lack of "bravery" to try superblock solution in a more heavy traffic area in the city center..?

#**TRASH HUNTING**

![](assets/trash.jpg)

Barcelona Trash Days means collecting tons of materials from the streets. In every district on a particular day all kind of trash can be thrown away (that means also a lot of people who move in here do not buy furniture from Ikea only collect them from the streets) - and these things are often fullworthy materials or pieces of furniture. Interestingly, each district has its own "DNA" and this can be determined by these objects being thrown away.

#**FLOTING THOUGHTS**

- Design for the future needs a context.
- Assumtions about Master Course vs reassesment.
- The same about Poblenou.
- To learn a city one needs to experience the city - go out, talk, try, take a look.
- Poblenou and lack of its public life.
- Things are happening behind closed doors. How?
- How to create the group of interests within those spaces in the city?
- It's a grand vision.
- It's not wrong to start changes small.
- Designing for and with human body.
