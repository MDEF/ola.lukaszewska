#Atlas of Weak signals
________________

I have to admit, at the beginning I misunderstood the terms of weak signals. I saw weak signals as some sort of cracks on the surface, metaphorically speaking. I read weak signals as events, happenings, movements or basically anything that is going on emerging now slowly, being in its own negativity, starting to scream "hey, I might be a problem if you don't react fast enough". But rather as being ruptures, weak signals are signals of change - phenomena occurring nowadays that have potential to be a "bigger thing". Weak signals indicate either new perspectives for well know subject (like new not yet existing jobs) or generally new subjects. After a series of lectures and readings we have had, I was left with the thought that such signals can be extremally easily overlooked, if only because they require a look at certain phenomena not by our present beliefs, but by a completely changeable perspective, non-biassed with our common assumptions. Luckily, they are people observant enough, skilled in spotting such signals.

![](assets/cracks.jpg)

During the series of lecture we have been introduced to the identified weak signals and the go as follow:

**Atlas of Weak Signals**


Life in the times of Surveillance Capitalism

- Attention protection
- Dismantling filter bubbles
- Circular data economy
- Truth Wars
- Redesigning social


Designing for the Anthropocene

- Climate conscience
- Inter-species collaboration
- Long-termism
- Carbon neutral lifestyles
- Fighting Anthropocene conflicts


Life after AI - the end of work

- Technology for equality
- Fighting AI bias
- Imagining new jobs
- Making Universal Basic Income work
- Human-machine creative collaborations


After the nation-state

- Making world governance
- Rural futures
- Pick your own passport
- Refugee tech
- Welfare state 2.0


Kill the heteropatriarchy

- Non-heteropatriarchal innovation
- Imagining futures that are not western-centric
- Reconfigure your body
- Gender fluidity
- Disrupt ageism

__________
_________


***A weak signal is an indicator of a potentially emerging issue, that may become significant in the future. Weak signals supplement trend analysis and they can be used to expand on alternate futures.*** *(Mikko Dufva)*

In physics, time is a fourth dimension. Some physicists say that spacetime can be understood as a stretchy fabric that bends due to different forces, such as gravity for instance. So what if (as a designers?) we see one of the forces being the weak signal? In our regular understanding of time, the future has billion tomorrows - because all the possible events depend on the actions of today, as in our acts shape the future, at least in the linear concept of time. Finding such change grains and correctly identifying them allows for earlier action on a given topic, because these signals are not currently booming problems, but require some time to "grow up". I think for a designers, especially us from this course and similar, working in the fields of emergent futures, it's crucial to understand those changes - not the trends, but those weak signals. And that is a matter of timeframing. Trends operate on a smaller (probably not only) time scale than those seeds of changes, they also - at least that is my feeling - operate on a smaller and not so enormously intersected fields.

![](assets/mindmap.jpg)
*scraping mind map by [Saira Raza](https://mdef.gitlab.io/saira.raza/) based on the sources and keywords we gathered along the course* - showing exactly what I mean - weak signals are complex
___________
___________

![](assets/mindmapgeneral.jpg)
*map of identified weak signals by Sitra’s Weak Signals*

________________
__________________
Personally, most impactful lecture was for me the one we talked about *Designing for the Anthropocene*, about redesigning the planet "for better", either by political actions, new materials, social innovations - generally increasing responsibility for what we give as an input to our world. Of course, these themes are connected with my final project, were they both talk about collaboration between species and ending the era of minds thinking we own this place because sadly we as humans have left an overwhelming evidence of our existence. But this topic also seems to me to be an extremely significant signals area, because it tells what may happen to our planet and which world we will leave after each other and how we will prepare it for future generations (on the not quite side note, by the way of future generations, I learned about this action - [Youth Climate Lawsuit](https://www.ourchildrenstrust.org/juliana-v-us) - click to read more on this topic). These are dilemmas very close to me, at least on the level of everyday worries, which is why these classes were important to me. I assume that in a sense the output of these classes was that if a project fits into one of the weak signals, then it has a better chance of being significant, having an impact and actually able to start changes or add to the pool of projects in a given area.

**"*Awareness allows us to generate a range of possibilities of what could happen, what we think should happen and what we might want to avoid which in turn can help us take more informed actions towards shaping the future*"** *(Jason Swanson, knowledgeworks.org)*

As far as my personal final project goes, I may need some guidance in the next term as to where exactly place it between the week signals - and how to do it properly - but after this AOWS course, I am starting defying those, because the math is simple: the more precisely described weak signals, the better the intervention. There are some events and phenomenas around our natural ecosystem that requires instant actions. But mind changing and shifting is something that needs to mature. The field of bio-design I am interested in includes the use of synthetic biology. SB invites a possibility, a danger, of disrupting natural ecosystems. As for now, the potential benefit and the urgent need to reform current practices towards and approach more in the tune with natural bio systems, far outweigh these risks. (based on[*(BIO DESING)*](https://www.amazon.de/Bio-Design-Nature-Science-Creativity/dp/0500294399/ref=sr_1_5?__mk_de_DE=%C3%85M%C3%85%C5%BD%C3%95%C3%91&keywords=bio+design&qid=1556138053&s=gateway&sr=8-5)). And so for my intervention I would place the project around the topics that I mentioned in my Design Dialogues post, weak signals of education and around those speaking about the future of bio-thinking like climate conscience and inter-species collaboration - perhaps not directly because the project is answer to those keypoints, but because its aim is to provoke more creative and critical thinking.

______________
__________________________
<iframe
  src="https://embed.kumu.io/d63698d2aac84803bdd4dba1cc6bfbaf"
  width="940" height="600" frameborder="0"></iframe>
_____________________
*team-work weak signals map, connecting the project's topics of participants (biodesign, new materials, food, bio-wastes) with "bio-influencer" as an intervention idea"*



_____________________
______________
Food for thoughts

  - [Timeline of the Near Future](https://en.wikipedia.org/wiki/Timeline_of_the_near_future)
  - [To Understand Your Past, Look to Your Future](http://nautil.us/issue/36/aging/to-understand-your-past-look-to-your-future)
  - [FUTURES LITERACY LAB FOR EDUCATION](https://www.utu.fi/fi/yksikot/ffrc/julkaisut/e-tutu/Documents/FFRC_eBook_3-2018.pdf)
  - [The Thing From The Future](http://situationlab.org/project/the-thing-from-the-future/)
  - [The Future of Humanity by Michio Kaku](https://www.youtube.com/watch?v=HKgURBN8sRY)
  - [Michio's Kaku 10 predictions for the future](https://www.youtube.com/watch?v=auyEhpJ71Ys)
  - [The Guardian view on Greta Thunberg: seizing the future](https://www.theguardian.com/commentisfree/2019/apr/23/the-guardian-view-on-greta-thunberg-seizing-the-future)
  - [How to make sense of weak signals](https://sloanreview.mit.edu/article/how-to-make-sense-of-weak-signals/)
  - [Biodesign Challenge](https://biodesignchallenge.org/)
