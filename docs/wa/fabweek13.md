WILD CARD WEEK

For this wild card week the topic of workshop was “welding”. Welding is a fabrication process that joins materials, usually metals or thermoplastics, by using high heat to melt the parts together and allowing them to cool causing fusion. During the class we were using MIG method - gas metal arc welding. Here is a video covering the general topic of welding, including the MIG method, exlpaining the difference between welding and soldering etc.

<iframe width="1519" height="586" src="https://www.youtube.com/embed/ZZvMsnSUDqo" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

The two essential ingridients for welding are heat for melting pieces of metal and protection from environment for preventing the impuriting of the melting piece. In MIG welding methods the heat comes from electrical arc between electrode wire and the workpiece metal, which heats the workpiece metal, causing them to melt and join.

![](assets/MIG_1.png)

GMAW torch nozzle cutaway image. (1) Torch handle, (2) Molded phenolic dielectric (shown in white) and threaded metal nut insert (yellow), (3) Shielding gas diffuser, (4) Contact tip, (5) Nozzle output face

After preparing the material, there is one step left. To make the work safe, you have to make sure you wear protection (welded metal can be ouch really hot), you eyes are protected (there are special mask for that purpouse), inform everyone around that you’re about to weld so their have time to go away or cover the eyes (shouting “READY?!” works pretty well). Once done, proceed to welding.

![](assets/IMG_4538.gif)

To weld two pieces, followe the seam, move the gun slowly in circular movement, continously. You have to be careful not to leave the gun for too long in one place, as the melting may create a whole in the material. Once the welding is done and no adjustments need to be may, next step can be taken.

![](assets/IMG_4541.JPG)

In the meatime, there is a video with incredibly beautiful welds.

<iframe width="1519" height="537" src="https://www.youtube.com/embed/diheuITIyKw" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

The final touch is to sand the welded material. Again, safety first - make sure to wear safety glasses as the pieces of metal can be superhurtfull for the eyes and skin. This process allows to have a clean surface of material but also check for any potential mistakes that could have been made while welding (i.e. holes).

![](assets/IMG_4548.jpg)


Media: Ryota Kamio


________________________

Possible applications using scraps from the workshop and MIG method - a skateboard. I cut the pipe in same hight pieces, arranged them in a skateboard-like shape and welded points of thouch on both sides. The track were then screwed to the board. It is surprisingly resiliant and possible to ride on. And also - no grip necessary :)


![](assets/weld1.JPG)

![](assets/weld2.JPG)

![](assets/weld3.JPG)

![](assets/weld4.JPG)
