#Designing for the new
(by Holon)

_________________
##Day 1 - Approaching the Practice

During this session we were talking about how do we trigger reflections and conversations about of planned practices.

A practice is *‘a routinized type of behaviour which consists of several elements, interconnected to one other: forms of bodily activities, forms of mental activities, ‘things’ and their use, a background knowledge in the form of understanding, know- how, states of emotion and motivational knowledge.’* (Reckwitz 2002a:249).

**ACITIVITY OF DAY ONE**

Deconstruct, in the group of interest, in my case what we called "collective learning", our intervention - by marking the actors-network, agents that the intervention affects, objects, discourses. Then, map the connections between actors and name the activities and how do they relate to each other.

Here is the map we came up with:

![](assets/DSC05235.JPG)

Based on this, choose one of the "everyday activity" and deconstruct it, marking objects, skills and images.

![](assets/holon1.jpg)

Taking a part in biology class seems like a simple, 45-minute unit kind of activity with few components making it a whole. But once deconstructed into even only categories and not to mention the actual deconstruction, saying that it has many agents and actors, shows the complexity of it. The understanding of actors of the activity that is aimed to be changes helps make sure to address all parts of it in order to be able to transition it into desirable future. Images can vary depending on each individual person, group, class, teacher or school, so each of the phrases can branch out into even smaller, individual threads. These images also talk about the skills that are required to complete them, take part in them or carry them out. Again, the level and advancement differ, the role of the actor and address are also different, that is, in the education system (the bigger umbrella under which the biology lesson is hidden), not only teachers and students, but also parents, the management, the ministry of education, organizations that create extracurricular activities and others. Objects will also not always be the same, very much in this matter depends on their availability and material status, but also on the approach of the school and creativity of parents. This only shows that you can approach the way of intervention from many parties. You can address each individual component of the deconstructed activity, and the angle of address can also be affected by how the intervention is carried out. I think this is valid because depending on the purpose the project and intervention should focus on a slightly different activity, and thus aim at specific objects, images and skills, and not try to solve "everything", especially in the first years of its life.

________________________________________________
##Day 2 - Designing the transition pathway

![](assets/holon2-02.jpg)



_________________________________________________
##Day 3 - Materializing the intervention

Rules:
1. Doing not talking
2. Playing seriously
3. Use what you have

1.Groups explore questions for exploration
2.Groups create a starting point (situation)
3.Groups create a quick story draft (storyboard)

**ACITIVITY OF DAY TWO**

Activity of the day was somewhat similar to improvised theatre. The group had to draw a storyboard about one of ours intervention designs and then, as rules above state, play it out using things we can find around us, not talking (and if, it shouldn't be in exchange for doing but perhaps as an extra additional feature) and taking the whole situation seriously (which at times was hard, as one of the girls was playing an overly enthusiastic mushroom). Next round, we played again only this time the spectators, other groups, could interact, adding new objects to the story or inserting themselves into the timeline of the impro.

![](assets/HOLON1.jpeg)

![](assets/HOLON2.jpeg)

![](assets/HOLON3.jpeg)


**MATERIALIZING INTERVENTION**

At first, I created a draft of a step-by-step scenario of activities for both me on the position of tutor and participants played by my MDEF colleagues. Based on HOLON’s instructions (although not completely followed, as the group had to talk and instead of making it a improvised theatre for intervention, I much more needed to actually try out the narrative, wording and basically, breaking out my personal barrier of speaking out loud) I created the storyboard of actions (sketch below) and finally played the workshop. Speculative tools, like using kitchen equipment instead of a lab’s, and glow-in-the-dark paint instead of algae helped visualize both tools and process. Eventually, I gathered the feedback from students and implemented it into creating a new plan of workshops.

Storyboard:
![](assets/storyboard2-02.jpg)

Workshop:

![](assets/intervention.jpg)

![](assets/intervention-7.jpg)

![](assets/intervention-2.jpg)

![](assets/intervention-3.jpg)

![](assets/intervention-4.jpg)

![](assets/intervention-5.jpg)

![](assets/intervention-6.jpg)

![](assets/intervention-7.jpg)

![](assets/intervention-8.jpg)

![](assets/intervention-9.jpg)

_______________________
##For reading

Design fictions ) - "awarness of change is what distinguishes design fictions from jokes about technology, such as over-complex Heath Robison machines or Japenese chindogu (weird tools)..." - to chalenge the status quo about things we surround ourselves with and the effects they have (Bruce Sterling)

![](assets/chindogu.jpg)

Also, this essey: https://www.wired.co.uk/article/patently-untrue
