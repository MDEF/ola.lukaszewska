#**WEEK05. Navigating Uncertainty**

*The uncertainty principle is one of the most famous (and probably misunderstood) ideas in physics. It tells us that there is a fuzziness in nature, a fundamental limit to what we can know about the behaviour of quantum particles and, therefore, the smallest scales of nature. Of these scales, the most we can hope for is to calculate probabilities for where things are and how they will behave. Unlike Isaac Newton's clockwork universe, where everything follows clear-cut laws on how to move and prediction is easy if you know the starting conditions, the uncertainty principle enshrines a level of fuzziness into quantum theory* [(source)](https://www.theguardian.com/science/2013/nov/10/what-is-heisenbergs-uncertainty-principle).


If even the smallest scale in nature has the described rule of ambiguity and the only option of "predicting" is to describe the probability of occurrence, then the scaling of this phenomenon (not numerically described, but talking about what we humans call the future) gives the assertion that the world is unpredictable and uncertain - just like the molecules that build it.

The designer must find himself in this uncertainty. On one hand, it's a simple survival instinct - not get lost - but on the other, creating a framework allows for more reliable navigation through unknown, and thus asking more relevant questions, conducting more accurate research, and finally, I hope, creating solid projects (although of course "solid" in in this case has many meanings). Because first you have to realize that you are lost before you start asking for directions.

___
##Futures

My favorite examples of trying to "preach" the future are visions from, for example, the '50s or '60s - if only because many of them talk about the world after 2000, so I'm in this (privileged?) position to verify them, in a way. Some of them are still hot topics nowadays - like electric, driverless and autonomous vehicles. Others seem completely recalculated, like a weather control machine. The next ones are interesting because they didn't make it today in terms of form but some part of those visions are happening in modern times, such as mail not delivered by a man - is this not a story about drones from Amazon? Some of them from today's perspective seem a bit funny, maybe even exaggerated, but if you look at those ideas closely, then despite having missing the reality (for example, the operation of some systems, the development of robotics, or rather its level of domestication) it seems that they gave very specific questions and therefore seek to identify challenges that await us (or in that case our ancestry) and identify the actions that must (have been) be taken to meet these challenges.

The pictures show: electric vehicle (1957), panel for controlling the weather for farmers for 2000 (1958) and fast post of tomorrow (1919).

![](assets/paleo-03.jpg)


A lot about predicting possible and likely futures is being said on the occasion of speculative design. I don't want to dwell here on the field itself and projects, but only indicate how the future and its features are used as a time frame, a tool, and maybe even as a brief.

One of the most classic examples I know is the book "Speculative everything" by Dunne & Raby. They describe the use of the future in design as opening perspectives on what is coming and on the so-called "wicked problems", as creating space for discussion, as a way to debate about alternative solutions, for redefining our relationship with the present.

It seems to me that in order to create projects for the emerging futures I must first understand why and how these possible and probable times are described and how they resonate with each other, what are their dependencies - what is different between what we want to happen and what is possible? Speculative design comes to the rescue (surprisingly, also the methodology of strategy planning by the military - hence the creation of scenarios). This diagram shows different kind of potential futures and the relationships or frames between each other.

![](assets/pppp.jpg)
*Described by Stuart Candy, illustrated in "Speculative Everything" by Dunne & Raby*


I once took part in the lecture by Zuzanna Skalska, analytic (I hope it's a proper word) of trends in design, innovation and business. She talked about methods of predicting trends and creating future scenarios. The technique that she was talking about was the STEEP analysis - each letter corresponds to one factor that influences the way the world is and will be shaping - SOCIAL, TECHNOLOGICAL, ECONOMICAL, ENVRIONMENTAL and POLITICAL factors. Although the analysis of trends is not, of course, the same as the design for emergent future or generally the talk about the future itself, I would like to draw attention to one of the elements. It was the subject of the first lecture this week.

___
##Environment

Ever-growing buzzwords of last years have been "environmental changes" - everyone heard of it, everyone read something about, everyone can even give some statistics and numbers. We all know that. But what all that actually means? Are we really that screwed up?

During the lecture we were presented with a lot of facts about climate change, such as CO2 content in the atmosphere, killing the coral reef, rising sea and ocean waters, and - somehow the saddest thing for me - the extinction of many species of animals. World Wide Fund for Nature experts estimate that the extinction of species nowadays oscillates between 1,000 and 10,000 times more than natural extinction rate (the rate of species extinctions that would occur if we were humans) [source](http://wwf.panda.org/our_work/biodiversity/biodiversity/).

![](assets/wwf.gif)

*That's a graphic published on WWF website.*


I was trying to wrapped my head around it and was left rather doomed and overwhelmed after all. If the changes are already in motion and if there is so much to be done, if it's all is so big - what can I do? And why is the climate change not making it to the first pages of newspapers? I dare to guess - and my thought was partly confirmed during lecturer, as he said a similar thing - that the scale of the problem is so huge, so complex that it leave us hopeless. The other thing is, there is no real political market or that idea or problem.

First of all we as designers should really think in favour of those who can't - animals, children that have no voice yet, children that are not born yet, but also those who live in ignorance or unawareness. We are responsible for the futures for those who will come after us. The motion between generations is essential. Where we as designers have power and what could have possibly the biggest impact is designing for creating new habits - we do not need more products, more things, but rather interventions. That's why services and systems designs are or may be so important, especially for the upcoming times. So the question is - how can we ship the perception in order to have a certain conversation already?

The voice of changes was rised through the "Our Chilldren's Trust" initiative. Their mision is to advocate on behalf of youth and future generations for legally-binding, science-based climate recovery policies. More on the video below [(*source*)](https://www.ourchildrenstrust.org/mission-statement/).

<iframe src="https://player.vimeo.com/video/127181835" width="640" height="360" frameborder="0" allowfullscreen></iframe>


**FLOATING THOUGHTS**

- Time scale. What is the present, what is the resolution that we operate in? The beginning of "now" can vary depending on the different scales we think of, on different times we refer to. There is "now" as this very only moment, there is "now" as today, "now" as times after 9/11, "now" as a times of "Anthropocene", "now" as times after agricultural revolution. Those moments are the *discontinuities of now*.

- Food industry is a big part o climate changes such as the impact of pollution through fossil fuel usage, landfill consumption or even animal methane. But there is another side of that industrialized food industry, besides environmental impacts - chickens that we know and eat nowadays is "*mad man out of space"* because it has been so modify over the years to fit the worst and worst conditions, that it's now a completely new species (in a way). It's purely a creation of Anthropocene, meaning it's nothing like the chicken from before.

- Minister of the Future. A figure of Thimoty Morton and his approach towards changing the relation that human have with nature and explains objects so massively spaced in time and space to go beyond location, such as climate change and styrofoam. Interesting interview with Morton can be found [here](http://lab.cccb.org/en/tim-morton-ecology-without-nature/).

___
We live now in an era of long term problems but short term politics and that creates a dissonance and disharmony. Some time ago I couldn't understand why out of sudden climate changes became political. The goodness of our Planet should stand above us, the future of generations should be a priority, protecting what is left from what we've been destroying shouldn't be a matter of one man in a big, white house. In 2017, after Trump's actions on The Paris Agreement Jeremy Jones, the founder of [Protect Our Winter foundation](https://protectourwinters.org/) and pro snowboarder, wrote an open letter to president, expressing his ager at not respecting facts and science (to start with). He writes:

*I ask them (children) if they want to leave the planet better than they found it. I ask them if they want to see a vibrant, thriving ocean or a bleached, dead one. Plowed fields or rain forest. Unfortunately, these talks have taken a somber turn. I have to decide if I should tell them the sobering truth that the United States of America is failing on climate. That big oil and big money are winning or that our elected officials are being funded by the fossil fuel industry and voting to protect the profits of a few, at the detriment of the planet.*

Now, it's a powerful message coming form a person who professionally cope with nature and spend almost the whole life in the mountains, whose work depends on the quality of winters and being in tune with the weather. Because one doesn't need science to notice the melting of glaciers, to notice migrations of whole cities because of sea level rise. Climate change and environmental issue must be political. Must be political in the sake of our children. Those matters need to become political so the decisions, the turns, the responsibility can be taken globally and may provide long term solutions to make the problems - with time and patience - short term. So it's not only technical or technological problem, but it became cultural and moreover, political.


[![](assets/winter.jpg)](https://www.snewsnet.com/news/jeremy-jones-paris-open-letter-to-trump)


___
![](assets/ryba.jpg)
*perch fish in anemone - example of symbiosis and mutual benefit*

In counteracting the feeling of helplessness in the vastness of changes that are coming and in the amount of work that must be done, the voice of Mariana Quintero appeared, telling how she perceives these changes as a positive process, in a sense.

Nature as a system doesn't work on the principle of struggle for survival and competition, as Darwin said, but on the basis of correlations, mutual dependencies and, above all, cooperation. This in turn may enable the creation of a new type of pact, protocol between organisms - if people take care of a particular species and care for providing benefits to a given species, this organism may repay by facilitating some aspects of human life (based on the principle of good, solid relation and not wasting resources!) and maybe even switch to mutualism and create a relationship in which one organism can not survive without the other (but without human egoism within).

When talking about a devastated environment, I always think at some point that as a designer I should focus on fixing this, on designing sensible solutions that have the potential to "save the world" and that very often it can mean giving up what is at work that I have pleasure doing or work in disagreement with my interests. Mariana in her speech quoted a [work](http://donellameadows.org/archives/leverage-points-places-to-intervene-in-a-system/) in which the author describes various levels and design layers depending on our network of interests. Mariana later wrote in e-mail to us: "*what it's interesting to me is how based on her experience and work in the field she can organize them depending on the level of impact they can create, (admitting of course that this hierarchy can be fluid relative to the system itself)*". But the low place in this hierarchy does not mean bad work. This allowed me to get out of class with a very good sense and feeling in a way.


___
##Unfolding the political capacities of designs

*Disscussed text can be found [here](https://www.researchgate.net/publication/265467472_Unfolding_the_political_capacities_of_design).*

Those videos show a quick, graphical summarision of the foregoing academic work. In a way they helped me simplify the text and calmed my minds, as I tend to run away from everything that contains the word "politic" (at this point I am adding "understanding politics" to my "needed improvements" list) without really trying to figure out at least what I can understand.

<iframe src="https://player.vimeo.com/video/179353602" width="640" height="360" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>


*The aim of this essay is to explore some of the ways in which design can matter politically. More specifically, we want to explore the capacity of design to reorganize what counts as political in our everyday lives. The most usual way to explore this question has been to focus on what we would like to call the 'enfolding capacities' of design. That is, on the capacity of design to inscribe, congeal, or hardwire, different political programs and power relations into materials, spaces and bodies.* (from introduction to publication)

![](assets/political.jpg)

![](assets/chair01.png)

*School chairs as enfolding mechanisms of disciplinary power. Lithography of H. Lecomte, 1818*


**FLOATING THOUGHTS**

- Can design construct or procude society? What is a matter of society, the social construct? What is design in society, what is its role there?

- What the chair? Specific alignment between subject and object. Chair is only a chair when social idea is said to us - as long as this idea exists, we assign specific form and function to it.

- Speedbumps are effective not because the driver thinks it's necessary to slow down in order to protect pedestrians, but only to protect the landing gear - so what is that drives the society, what are the motives and decision making processes? Also, is it morally proper to design knowing (or guessing) the motives might be "egoistic" instead of emphatic, but the goal of design will be reached? Does the same principle applies when the process is scaled up and apply not the product as a "thing", but society? Should the social responsibility (or fear?) be translated into personal, sort of egoistic concern or should we rather focus on trying to start up a conversation and change that attitude?

___
##Resonation. Reflection.

We did an exercise of trying to create a 'statement' on how the last week lectures resonated among us in terms of the vision from previous week, balancing between abstract descriptions and solid anchor points aka more specified terms.

Points of interest and thoughts from week 05:

- The strongest impact I felt after environmental changes class in terms of desiging for future generations and for unknow world. Design must predict its own consequences, regardless of the scale and resolution of the time in which it operates.

- Based on speedbumps example, I want to understand the process of decision making and the process of motivation in order to be able to implement that knowledge into designing an intervention, as in the context of part of my vision last week and the issue of designing workshops and workshop tools.

- "All physical systems are based on an exchange of information". The exchange happens everywhere. I felt connection to this sentence as it speaks about physic and the basic rule of universe and that is how I started this week's note. The exchange of information between humans can be use as a tool for the involvement, emancipation and distribution of knowledge, which resonates with the name of group o interest from bootcamp as well as with my wheel of interest from last week.

- There is a huge responsibility in what and how we design. We are throwing ideas into the world and there is a huge possibility they will - in a way - start to live their own life. When we create an interaction, it may starts with just few people, but society is a huge network and is rarely immune to influence and exchange of information - or relations, interactions etc, so the idea is quite likely to spread. And ideas may distribute power or influence and therefore is really political.

I want to finish this week's notes with plan for next week - I am about to go to my wheel of interests and rethink it by adding different dimensions - for example describing scales in which I want to operate or even trying to prioritise and hierarchize the graph to hopefully find a focus of concentration and start describing my ideas for the project.

Last, but not least something to reflect on during this term and probably well after - do we need to ask how designers can navigate uncertainty where the uncertainty is the subject or maybe rather how to  navigate through uncertainty, for it, within it...?


___
##Collections

[Leverage Points: Places to Intervene in a System](http://donellameadows.org/archives/leverage-points-places-to-intervene-in-a-system/)
