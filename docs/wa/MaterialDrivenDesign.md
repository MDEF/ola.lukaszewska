# Material Driven Design
*by Mette Bak-Andersen (KEA Material Design Lab) and Thomas Duggan*

______


***"By harnessing the unexplored potential of materials we can implement social, economical and political change"***
(*Matter*)

______
##Beer brewing by-products

**Spent grain** and **yeasts** are the by-products of the distillation process. *Brewer's spent grain usually refers to barley produced as a by-product of brewing, while distillers grains are a mix of corn, rice and other grains.* [(source)](https://en.wikipedia.org/wiki/Distillers_grains). The one we got from Poblenou-located brewery, Birra08, was barley.


<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2992.4808301537655!2d2.1887493148644874!3d41.407079902738005!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x12a4a32445cfe629%3A0xdbaca69614c56b52!2sBIRRA+08!5e0!3m2!1sen!2ses!4v1552683314469" width="1000" height="400" frameborder="0" style="border:0" allowfullscreen></iframe>


At the begining I present different bio waste (spruce needles), but after giving it a go and after Mette's suggestion I gave on it and joined the team working on described here in this documentation. Here is some information from [Julia](https://mdef.gitlab.io/julia.danae/reflections/Material-Driven-Design/), who arranged the meeting with brewery, about Birra08 and their biomass:

*Once a week on Tuesdays, they start the process for beer making which can take up to a week depending on the beer. Every week, 300 kg of spent grain (which is used malt), or 15 tonnes a year is obtained in the process in this micro -brewery. (...) Out of curiosity I asked how much water is being used in this process of production and I have been told that the water they need to use for the amount of litres of beers they want to get needs to be at least three times bigger. They use about 3700L of water, which corresponds to about 1200L of beer produced per tank. Approximately, 4500L of beers is produced weekly at Birra08.*



**Spent grain**
At the right moment, the grain is dried, and thus stops the internal life processes, it is sprouted (the sprouts are mechanically removed), and then the 4-6 weeks drying-out of the grain (ripening of malt) is carried out.  

The grains are put though a mashing process, where grain is ground and added to hot water. The starch in the grains undergoes saccharification by enzymes, turning the starch into sugars that are released into the water. The water is removed from the grain and becomes wort for brewing. The remaining grain, called "spent grain" for the removal of simple sugars and starch, can then be sold as a by-product as a mass for feeding farm animals.

The general ingredients of spent grain: protein, fibre, fat and up to 70% moisture.

By the end of experimentation and creating a powder I notice that the spent grain, even if mildly blended, can be divided in yet two more separate ingredients, the grain itself and the fibre part of it. It has a significant difference in weight.

![](assets/DSC02763.JPG)


**Yeasts**
I've found the information that the development of brewing technologies and microbiological techniques has resulted in the selection of special varieties of brewer's yeast not having their counterparts in nature. It was influenced by fermentation technology characteristic of brewing, which used yeast from one fermentation to initiate the next one [(source)] (https://pl.wikipedia.org/wiki/Dro%C5%BCd%C5%BCe_piwowarskie). And that means the yeasts that are consider waste are not "live" anymore - they cannot provoke another fermentation, but they are high in proteins and some vitamins and that's why the yeast slime is given or sold to farms - it enrichen cows' milk.


________
##Working with material - handbook


Allow questions to come. Allow time. Document every step of the way.


**Get to know your material - explore the possibilities**

Don't focus on your final product just yet. Try to learn your material, pure at first, to be able to choose and use suitable and proper tools.

Try to:

- blend it,
- crash it,
- squeeze it,
- dry it,
- bake it,
- cook it,

Ask yourself:

- can it become fluid?
- can it become fibre?


**Experiment**

Go into testing in 3D shapes as soon as possible - it will give more insights into material features - so don't work only with plain sheets, especially in the further steps. If you notice any particular feature, try to perform some experiments and tests to enhance it. Try to notice weaknesses as well and work on them.


**How to work with pulp**

Check properties of binding on its own - blend it (while still wet) and dry it or first dry it, then blend to powder, add water and dry again (sometimes it can give different results). If the material have fibres, it may be good to test different "fines" of the pulp (or powder). If the fibres are wanted, it's good to either dry the pulp first and then mechanically extract the fibres (the method depends on the length of fibres) or steam the dry material before extracting fibres. If it doesn't bind on its own, think about what binders could be used - Internet is full of recipes for natural glues - for example milk glue. Another way of getting binders is to visit art supplies stores - there are plenty of binders that are used to priming canvas, like gelatine solution or bone glue - in the specific case of painting this operation gives the canvas a little stiffness and elasticity.


**Mimicking techniques**

Sometimes we want to test material in the context of specific technique. In most of the cases it would require either getting expensive equipment or learning how to use it. As much as exciting and perspective - opening it is, it's also time- and sometimes resources - taking. To save the trouble it's good to mimic those techniques, for example: if the aim is to 3D print with given material and we want to test it in this context, the goal would be to either build 3D printer or extruder itself, then maybe use a robotic arm. However there are bunch of parameters of the material that need to be known before using it that way. So for first prototypes translating those techniques into way simpler tools is a way to work with - instead of using robotic arm, lets use our own arm and instead of extruder, lets use syringe. We take out from the equation a lot of parameters that we don't need to worry about at first and we can focus on chosen features of material.


![](assets/glasses-6.jpg)

*Quite a fail mimicking 3D printing (extrusion in water - it wasn't a perfect medium to extrude in and not a perfect tool to extrude with - it let the material set up quite quickly but also dissolved it a bit)*


**Controlling**

As mentioned before, it's important to write down and mark in a clean and precise way all the experiments, samples and tests. It makes later presentation way easier, too, but most importantly it also allows to repeat the experiments if one of the samples seems to have some interesting properties. It gives a control (or a tool to control) over experiments and material itself.

Secondly while working with material, especially in first phases, it's important to use as much material and as little binder as possible to learn to fully control and therefore understand (or: understand and therefore control) the pure mass itself.


_____
##Experiments

*Note before reading: all the drying processes in oven where done by 50*C (unless stated otherwise), by using term "material", I am referring to spent grains and not yeasts - yeasts waste is described in separate paragraph.*


**Self-bonding**

As mentioned earlier, the first test is to check whether the material can bond itself. I ran three tests, both as sheets and in spherical shapes:

  1. moist raw material, put in form, dried in the oven;
  2. wet material, blended, put in form, dried in the oven;
  3. moist material, first dried in the oven, then blended into powder, worked with as in the case of paper making (powder in water, filtered, drained water), dried in the oven;

None of these attempts gave results in the sense of a material that would stick with itself. I couldn't make a sample which wouldn't crumble when dry and wouldn't fall apart after taking it in my hands. This test showed, however, that the material needs additional glue, some connector, to remain in a solid form. Another conclusion is the observation that the dry material is comminuted to a powder much better than the moist pulp.


![](assets/DSC02772.JPG)



**Bioplastic**


![](assets/glasses-7.jpg)


The first attempts to create a mixture with the material began with recipes for bioplastics. At the beginning, I used recipes from Material Activism  - I assumed at the beginning that the grains have some, though barely, amount of starch in the composition, so the modified recipes had powder from grains instead of starch.


<iframe src="https://e.issuu.com/anonymous-embed.html?u=miriamribul&d=miriam_ribul_recipes_for_material_a" style="border:none;width:100%;height:500px;" allowfullscreen></iframe>


These attempts didn't work out well, that is, the material didn't present any specific properties - either it didn't bind, or - and mostly - disintegrated and fall apart. As it later turned out (after a deeper research), starch occurs in raw grains, but after the process of brewing the beer it's broken down and doesn't occur in by product waste.

Eventually, we used the recipe presented below.

![](assets/bioplastic-01.png)

Although the object itself retained its shape, dried out, it was not brittle, it was relatively fragile but most importantly the material content was very small, and so with these tests I couldn't extract the properties of the material, understand it, learn it. The very idea of ​​an object that doesn't draw from our material and doesn't have any specific benefits from it didn't seem encouraging and above all reasonable.


**Yeasts**

While working with the material, [Julia](https://mdef.gitlab.io/julia.danae/reflections/Material-Driven-Design/) discovered that after evaporation of water from yeast molasses, the yeast water becomes slimy and sticky and can serve as a binder. She together with [Ryota](https://mdef.gitlab.io/ryota.kamio/reflections/materialdrivendesign/) made some tests and mixed it with spent grain flour, pressed it and dried in the oven for more than 24 hours. The results were - according to them - really satisfying and they kept working with that process. I, on the other hand, wasn't able to repeat the process in order to have similar results. I haven't discovered if it was the proportions, drying process or creating a pressed form.

For a moment I focused my work on the yeast itself. My assumption was that if some yeast viscosity is removed, I would be able to create a kind of membrane, elastic material - this thesis came from observing the bowl with yeast slime which we left over the night  - the top layer dried up, it wasn't so much sticky and it seemed like it become flexible.

![](assets/DSC01007.JPG)


The next step was mixing yeast water with gelatine and glycerine in various configurations and proportions, which, however, weren't precisely controlled (and that was my rookie mistake) - that's why I couldn't tell if the experiment was the result of the yeast content or rather it behaved the way gelatine would behave without being mixed up with an additional medium. In the picture one of the tests first after concentration (setting up), then after completely drying.

![](assets/DSC00779.JPG)

The same mass, yeast water with gelatine (without glycerol), I also poured into molds. An interesting phenomenon was the different surface finish, but the error was the same - no control of the amount of ingredients, which of course had a huge impact on the final conclusions.

![](assets/DSC02655.JPG)

I repeated these tests, this time controlling the contents and proportions, measuring out the ingredients. I mixed evaporated water (slime) with dissolved gelatine in proportions (yeast : gelatine) 1: 5, 1: 2, 1: 1, 2: 1 and 5: 1. This led to the fact that the properties of the resulting material were derived from the properties of gelatine, not the slime. Here on the photo that part can be seen in ice tray.

![](assets/DSC02246.JPG)

It seems to me that the yeasts itself are extremely valuable and with somewhat meaningful potential, but - especially on my terms - very difficult to control, mainly because of their lifespan. In our tests, they were useful for creating a sticky slime, so it reduces the need of adding amounts of extra bonding component.


**Hybrids**

One of the tries I performed after noticing the biomass doesn't have self bonding properties was mixing raw but dried material (whole grains, not powdered) with (an uncontrolled) amount of bone glue (in volumic ratio 1:1), dissolving bone glue in water bath first. I put the mass later in a container with rounded corners and remove once it set up to let it dry freely in the air.

![](assets/DSC02764.JPG)

That was my base for future continuation with powder, although those samples didn't happen one after another and I had to remind myself about this try first to think about using bone glue with powdered spent grain.



______
##Product

One of the deliveries point was to present a "final product". Presenting the product at the end of the experiments is important because it requires taking a very specific direction - that is, strengthening selected features that will be important in the product, focusing on matching the material features to the selected shape. This is quite tricky, because even after 3 weeks of experiments and trials I wasn't able to control the material and know its properties in one hundred percent.

To present the final product, I chose my key samples (presented in the previous paragraph) and thought about the object that could present (need) similar properties. Considering the time frame that we had imposed on this project, I didn't want to spend a lot of time brainstorming about the product itself, but rather on testing already chosen one. And so I decided to make a frame for eyewear - the sample material was strong even in a thin layer, was mirroring the details of the form quite precisely and took time to set up completely, which made it easier to insert and remove from form before it became hard.



![](assets/glasses.jpg)

![](assets/glasses-2.jpg)


____
**The recipe:**

- 5 tablespoons of powdered dried material
- 10 tablespoons of lukewarm yeast water
- 5 tablespoons of lukewarm water
- 1 tablespoon bone glue

First, the bone glue was dissolved in water and yeast water by making a water bath and when fluid, powdered material was added. Later the batch was mixed and while already quite thick was pressed into the form and removed from it after 1 hour in the cold (kept in fridge). The object needed 24h+ to completely dry and become hard.

____
##Material driven design

Why is it important and useful to design new materials? Because those action can help reduce the impact we as human are making on the planet by producing certain materials, especially single-use ones. Such experiments, even if not always with greater results, can start a conversation, can inspire someone else to pick up the work, see it from another angle and all together, it creates a huge portfolio of possible new solutions, start points and stimulate future developments.

We live in a word made and created by materials. Every object, every item serves a certain purpose and yet not always the material corresponds with its goal. This changes within the material driven design approach, where the material plays the main role and instead of using materials for example just for aesthetics purposes, its features are main focus - it's similar to bottom up way of working.


_______
##Reflections

I would like to continue working on the material. Not necessarily because it has presented some amazing properties that will revolutionize the market, but because I have already met it a bit. I plan to try to focus the tests and production of further samples on the product I chose (but remain open to the possibility that pairing the material with this particular object is not the perfect combination) - strengthen the structure, add flexibility, which is needed for glasses, but also refine the aspects unrelated to the material itself - like sample planning and regulations, documenting the process, describing samples and their corresponding "recipes", and rework the mold (matrix).

One of the main reflection that stacked with me, especially after one if the first class when everyone was presenting bio-waste and describing the place of origin, was that there is a huge amount of material available around us and they are not always or actually hardly ever included in any kind of circularity in the city. If the waste serve a purpose it's usually to feed the stock, but not as a local source of potential new material that is to be commercialized (that part can be tricky too) that way or another but not used only for artisan kind of purposes.

Another thing - and that's the one that will be probably the most valuable for my Master project - is to always remember and consider time constrains, especially while working with organic matter - whether it needs to ferment, grow or dry. That's a huge part that cannot be ignored but also not always can be precisely predicted or planned, especially during the first experimental phases.


_____
##Embodied knowledge



![](assets/prezentacja.jpg)
