### About me

I'm Ola.

During my BA studies at Industrial Design Department at School of Form (Poznań, Poland) I was able to experiment, explore, understand and try.
Those were the activities that has allowed me to create different piles of interest and experiences - changing the process of production, creating new materials, comparing craftsmanship with robot-driven work, generative design, all in a spirit of human-centred design, balancing on the edges between physical and digital world.

My brief experience with transportation industry together with interest in cars in general lead me to focus my thesis on transportation systems in the era after the car. What caught my curiosity was - in such fast and constantly changing world - humans and their sensorium in the urban space. One of the question I'd got asked during my final presentation - how would I imagine an organic, soft transportation system scenario -  opens up a new chapter of interest for me: biomimicry and bionic.

I've read somewhere once that "design won't save the world, but it will make it look damn good". I strongly believe that designers, altogether with support from other disciplines, have the power to provide solutions to existing problems, to predict the upcoming and somehow really save the world with smaller or bigger undertakings and ideas.

Where do I see myself in 10 years?
I hope this particular Master Course would help me re-focus and solve the question "where do I see myself in 1 (or 2) year(s)". I wish that 10-years-older-me will work on projects that have environmental engagement, education, some sort of social responsibility and fairness at theirs cores, should that be re/designing playgrounds for kids, re/creating textiles or translating perishing craftsmanship into robo-made goodies.
I believe nowadays design steps up into quite a new era, where the definition of this profession has to be changed and cannot be longer identified as only aesthetic. In 10 years design will be quite a different movement, changing the world as we know it with responsibility for actions taken and I hope to be part of it.

Now, I do not have a particular talent or exceptionally strong skill, but I am extremally curious and I hope future me will never stop exploring, asking and being concerned.
